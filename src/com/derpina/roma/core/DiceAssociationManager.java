/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Matt
 */
public interface DiceAssociationManager {

    Dice getActionDiceOfValue (DiceDisc disc);
    DiceDisc getDiceDiscOfValue (Dice dice);
    int getDiscIntegerValue(DiceDisc disc);
    DiceDisc getDiceDiscOfValue (int value);

    boolean isBribeDisc (DiceDisc disc);
    DiceDisc getBribeDisc ();
}
