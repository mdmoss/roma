/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface ObjectBuilder {

    public GameCardManager createCardManager(PlayerManager playerManager, DiceDiscManager diceDiscManager, DiceAssociationManager diceAssociationManager);

    public TurnManager createTurnManager();

    public VictoryPointManager createVictoryPointManager(PlayerManager p);

    public DiceDiscManager createDiceDiscManager();

    public Collection<CardInstance> getInitialDrawPile();

    public Player createPlayer();

    public DiceDisc createDiceDisc();
    
    public Dice createDice();

    public DiceManager createActionDiceManager();

    public MoneyManager createMoneyManager(PlayerManager p);

    public GamePhaseManager createGamePhaseManager (PlayerManager p, VictoryPointManager victoryPointManager);

    public PlayerManager createPlayerManager(TurnManager tm);

    public DiceAssociationManager createDiceAssociationManager (DiceDiscManager ddm, DiceManager adm);

    public ObjectPropertiesManager createObjectPropertiesManager (GameCardManager gameCardManager);

    public TimeManager createTimeManager();

}
