/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import com.derpina.roma.backend.HumanPlayer;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface GameCardManager {

    Collection<CardInstance> getCards(Location location, CardType type);
    Collection<CardInstance> getCards(Location location, CardType type, Player owner);

    Collection<CardInstance> getOpposingCards(Location location, CardType type);

    void makeCardFloating(CardInstance card);

    void moveCardToDisc(CardInstance card, DiceDisc destination);
    void moveCardToDisc(CardInstance card, DiceDisc destination, Player player);

    void moveCardToDiscardPile(CardInstance card);

    void moveCardToDrawPile(CardInstance card);

    void moveCardToHand(CardInstance card, Player hand);

    void moveCardToTimeWarp(CardInstance card);

    /**
     * If the given object is associated with a player, (eg: on a dice disc
     * belonging to a player), return that player. Otherwise return null.
     *
     * @param card
     * @return associated Player or null
     */
    public Player getOwner(CardInstance card);

    Collection<CardInstance> getOppositeLaidCards(CardInstance card, CardType type);

    public Collection<CardInstance> getCardsOnDisc(DiceDisc disc, Player owner);

    public DiceDisc getCardLocation (CardInstance card);

    public Collection<CardInstance> getAdjacentCards (CardInstance card);

    public void addCardToGame(CardInstance card);
}
