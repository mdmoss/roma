/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import java.util.Collection;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface PlayerManager {

    // FIXME: Rename to getAllPlayers
    Collection<Player> getPlayers();

    Player getPlayer (int playerNum);

    Player getCurrentPlayer ();

    Collection<Player> getOpponents (Player player);
}
