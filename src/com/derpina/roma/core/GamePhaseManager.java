/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Matt
 */
public interface GamePhaseManager {

    boolean isComplete();
    Player getWinner();
}
