/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import com.derpina.roma.core.Player;

/**
 *
 * @author Matt
 */
public interface VictoryPointManager {

    void setPlayerPoints(Player p, int value);
    void setPool(int numPoints);

    public int getPlayerPoints(Player p);
    public int getPool();

    public void moveVictoryPoints(Player from, Player to, int amount);
    public void movePointsToPool(Player from, int amount);
    public void movePointsFromPool(Player to, int amount);

}
