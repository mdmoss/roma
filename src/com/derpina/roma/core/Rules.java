/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import java.util.Collection;

/**
 *
 * @author Matt
 */
public class Rules {

    final public static int STARTING_MONEY = 0;
    final public static int STARTING_VICTORY_POINTS = 10;

    final public static int NUM_PLAYERS = 2;
    final public static int INITIAL_TURN_NO = 0;
    final public static int TURN_ACTION_DICE = 3;

    final public static int STARTING_CARDS = 4;
    final public static int STARTING_TRADE = 2;
    final public static int NUM_DICE_SIDES = 6;


    final public static int TOTAL_VICTORY_POINTS = 36;
    final public static int VICTORY_POINT_DEFEAT_NUM = 0;
    public static final int NUM_CARD_FIELDS = 7;

    public static final int MERCATOR_SESTERTII_TRADE_AMOUNT = 2;
    public static final int MERCATOR_VP_GAIN = 1;
    public static final int MERCATUS_VP_GAIN = 1;

    public static final int ESSEDUM_DEFENCE_REDUCTION = 2;

    public static final int STARTING_TURN_NUMBER = 0;
    public static final int TRIBUNUS_PLEBIS_TRANSFER_AMOUNT = 1;
    public static final int CONSUL_INCREASE_AMOUNT = 1;
    public static final int CONSUL_DECREASE_AMOUNT = 1;
    public static final int BASILICA_VP_GAIN_INCREASE = 2;
    public static final int TARDUS_MAX_RANGE = NUM_DICE_SIDES;
    public static final int KAT_LIVES = 9;
    public static final int TURRIS_DEFENCE_INCREASE = 1;
}