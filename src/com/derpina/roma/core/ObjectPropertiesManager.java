/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.backend.modifiers.DefenceModifier;
import com.derpina.roma.backend.modifiers.DiscModifier;
import com.derpina.roma.backend.modifiers.TemporaryModifier;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Matt
 */
public interface ObjectPropertiesManager {

    public int getCostFor(CardInstance card, LocalGameState gameState);
    public int getDefenceFor(CardInstance card, LocalGameState gameState);
    public boolean getIsBlocked(DiceDisc disc, Player asking, LocalGameState gameState);

    public List<CostModifier> getCostModifiers();
    public List<DefenceModifier> getDefenceModifiers();
    public List<DiscModifier> getDiscModifiers();

    public Collection<TemporaryModifier> getAllModifiers();

    public void setCostModifiers (List<CostModifier> modifiers);
    public void setDefenceModifiers (List<DefenceModifier> modifiers);
    public void setDiscModifiers (List<DiscModifier> modifiers);
}
