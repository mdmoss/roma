package com.derpina.roma.core;

import java.util.Collection;

/**
 *
 * @author Lasath Fernnado
 * @author Matthew Moss
 */
public interface GameEngine {

    /**
     * Any User interface can call this to get a list of available intents to
     * present to the user.
     * Note that this is a blocking function, <i>please do not use it in your
     * UI thread</i>
     *
     * @return Collection containing all the valid actions a user can
     * choose, in the form of intents.
     */
    public Collection<Choice> getValidIntents();

    /**
     * A user interface can use this to convey the user's chosen intent to the
     * GameEngine.
     * Note that this will ONLY accept the intents that were handed out in the
     * last call to getValidIntents();
     *
     * @param a the action that was chosen
     */
    public void handleChoice(Choice chosen);

    /**
     * A user interface can use this to get the current state of the
     * game currently modeled by the GameEngine.
     *
     * @return GameState
     */
    public LocalGameState gameState();
}
