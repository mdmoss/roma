/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.core;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public enum Location implements Token {
    //ordered - index 0 is top of pile (i.e. card to drawn next)
    DRAW_PILE,
    //ordered - index 0 is top of pile (i.e. card that was last discarded)
    DISCARD_PILE,

    //completely unordered
    PLAYER_HAND,
    FLOATING,
    TIME_WARP,

    //presented in ascending numerical order without fillers
    DICE_DISC;
}
