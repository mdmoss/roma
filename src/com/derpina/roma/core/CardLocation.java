package com.derpina.roma.core;

import com.derpina.roma.core.Token;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface CardLocation {

    /**
     * The {@link Location} value that will match this in queries
     * by {@link CardManager}.
     *
     * @return enum value for this location
     */
//    public Location getEnumValue();

    /**
     * All the cards that are in this or any sub locations.
     *
     * Note: The card manager will respect any order this is given in when
     * building query results.
     * @return
     */

    public void addChild(CardLocation child);

    public CardLocation getChild(Token identifier);

    public Token getIdentifier();

    public void removeCard(CardInstance card);

    public Collection<CardInstance> getAllCards(CardType ofType);

    public void insertCard(CardInstance card);
}