package com.derpina.roma.core;

/**
 * Interface for access to any persistent Roma game data.
 *
 * @author Matthew Moss <mdm@cse.unsw.edu.au>
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface LocalGameState extends TurnManager, DiceManager, DiceDiscManager, PlayerManager, MoneyManager, VictoryPointManager, GameCardManager, DiceAssociationManager, GamePhaseManager, ObjectPropertiesManager, TimeManager {

    public LocalGameState clone();

    // It's quiet...


    // ...too quiet.
}