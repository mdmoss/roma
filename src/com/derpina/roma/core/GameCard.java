/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.cards.PlaceAction;
import com.derpina.roma.core.CardType;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface GameCard {

    /**
     * Called when a card is activated by the player.
     *
     * @return The next action to be executed.
     */
    Action getActivateAction(LocalGameState gameState, CardInstance card);

    int getBaseCost();

    int getBaseDefense();

    String getCaption();

    Action getDiscardAction(LocalGameState gameState, CardInstance card);

    String getName();

    int getNumDiceRequired();

    /**
     * Called when a card is placed on a dice disc.
     *
     * @return The next action to be executed.
     */
    PlaceAction getPlaceAction(LocalGameState gameState, DiceDisc disc, CardInstance card);

    CardType getType();

    CardInstance createInstance();
}
