/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface DiceManager {

    Collection<Dice> getActionDice();

    int getValueOf (Dice dice);

    void setValueOfDice (Dice dice, int value);

    void setNextBattleDie (int roll);

    Dice getBattleDie ();

    void maskDice (Dice dice);

    void revealDice (Dice dice);

    Dice getDiceOfValue (int roll);

    void useUpRemainingDice();

    void useUpDice(Dice value);

    void unmaskAllDice ();

    /**
     * Sets the values of available as given.
     *
     * Note, if less values are given than there are action
     * dice in the game, all remaining ones are set as used.
     *
     * @param values values of dice as integers
     */
    void setActionDiceValues(Collection<Integer> values);
}
