/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

import java.awt.Color;

/**
 *
 * @author Matthew Moss
 * @author Lasath Fernando
 */
public interface Player extends Token {

    public String getName();
    public Color getColor();

}
