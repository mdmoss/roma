/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.core;

import com.derpina.roma.backend.choosers.AbstractChoiceFactory;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class Choice {
    ChoiceType type;
    String description;
    Object value;
    AbstractChoiceFactory owner;

    public Choice(ChoiceType type, String prettyString, Object value, AbstractChoiceFactory owner) {
        this.type = type;
        this.description = prettyString;
        this.value = value;
        this.owner = owner;
    }

    public Object getValue() {
        return value;
    }

    public ChoiceType getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public AbstractChoiceFactory getOwner () {
        return this.owner;
    }
}
