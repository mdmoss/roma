/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public enum ChoiceType {

    END_TURN,
    ACTIVATE_MONEY_DISC,
    ACTIVATE_CARD,
    PLACE_CARD,
    ACTIVATE_CARD_DISC,
    ACTIVATE_BRIBE_DISC,

    CHOOSE_ACTION_DICE,
    CHOOSE_DICE_DISC,
    CHOOSE_CARD,
    TRUE_FALSE,

    CHANGE_ACTION_DIE,
    DECREASE_ACTION_DIE,
    }
