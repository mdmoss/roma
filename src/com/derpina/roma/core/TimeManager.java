/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Matt
 */
public interface TimeManager {

    public void schedule (Player player, CardInstance card, DiceDisc dest, int turn);

    public CardInstance getPendingCardPlacements (LocalGameState gameState, Player player, DiceDisc loc, int turn);
}
