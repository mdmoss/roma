/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Matt
 */
public interface TurnManager {

    int getCurrentTurnNo();

    void incrementTurn();
}
