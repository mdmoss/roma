/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.core;

/**
 *
 * @author Matt
 */
public interface MoneyManager {

    void earnPlayerMoney(Player player, int amount);
    void spendPlayerMoney(Player player, int amount);

    int getPlayerMoney(Player player);
}
