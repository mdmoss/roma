/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.ui.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import com.derpina.roma.backend.RomaGame;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.GameEngine;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class Fuzzer {

    public static int RUN_LIMIT = 1000;
    public static int TURN_LIMIT = 5000;

    private GameEngine game;

    public Fuzzer (GameEngine g) {
        this.game = g;
    }

    public void exec() {
        for (int i = 0 ; i < RUN_LIMIT; i++) {

            while(!game.gameState().isComplete() && game.gameState().getCurrentTurnNo() < TURN_LIMIT) {

                Printer.showGameInfo(game.gameState());

                Collection<Choice> intents = game.getValidIntents();
                List<Choice> l = new ArrayList<Choice>(intents);

                Printer.presentToUser(l);
                Choice hp = l.get(new Random().nextInt(l.size()));
                System.out.println("Selected: " + hp.getDescription());
                game.handleChoice(hp);
            }

            System.out.println ("****************************************************************");
            System.out.println ("turn No :" + game.gameState().getCurrentTurnNo());
            System.out.println ("Finished game " + i);
            System.out.println (game.gameState().getCurrentPlayer().getName() + " was victorius!");
            System.out.println ("****************************************************************");

            game = new RomaGame();
        }
        this.finish();
    }

    public static void main (String[] args) {

        GameEngine engine = new RomaGame();
        Fuzzer f = new Fuzzer(engine);
        f.exec();
    }

    private void finish() {
        System.out.println ("Played " + RUN_LIMIT + " games");
        System.exit (0);
    }
}
