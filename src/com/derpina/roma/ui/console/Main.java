/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.ui.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.derpina.roma.backend.RomaGame;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.GameEngine;

/**
 *
 * @author lasath
 */
public class Main {

    private GameEngine game;

    public Main(GameEngine g) {
        this.game = g;
    }

    public void exec() {
        while(!this.game.gameState().isComplete()) { //for now

            Printer.showGameInfo(game.gameState());

            Collection<Choice> intents = game.getValidIntents();
            List<Choice> l = new ArrayList<Choice>(intents);

            Printer.presentToUser(l);
            Choice hp = getChoiceFromUser(l);

            game.handleChoice(hp);
        }
    }

    private Choice getChoiceFromUser(List<Choice> l) {

        int choice = 0;
        do {
            System.out.print("Please enter a choice (" +
                                Printer.FIRST_CHOICE + ".." + l.size() + ") :");
            try {
                choice = Integer.parseInt(System.console().readLine());
            } catch (NumberFormatException e) {
                System.out.println("That is not a number!");
            } catch (java.lang.NullPointerException e) {
                System.out.println("This UI must be run in a standards "
                        + "compliant console. Sorry for the inconvenience.");
                System.exit(1);
            }
        } while (choice > l.size() || choice < Printer.FIRST_CHOICE);

        return l.get(choice - 1);
    }

    public static void main(String[] args) {
        GameEngine engin = new RomaGame();
        Main ui = new Main(engin);

        ui.exec();
    }
}
