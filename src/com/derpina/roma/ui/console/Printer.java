/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.ui.console;

import com.derpina.roma.core.*;
import java.util.List;

import sun.nio.cs.ext.ISCII91;

/**
 *
 * @author Matt
 */
public class Printer {

    public final static int FIRST_CHOICE = 1;

    public static void showGameInfo (LocalGameState state) {
        String color = "";
//        if (state.getCurrentPlayer().getId() == 0) {
//            //color = "\u001B[31m";
//            color = "";
//        } else {
//            //color = "\u001B[34m";
//            color = "";
//        }

        System.out.println();

        System.out.println (color + "Current Turn: " +state.getCurrentTurnNo());
        System.out.println (color + "Current Player: " +
        state.getCurrentPlayer().getName());
//      System.out.println("Cards Available in Draw pile: " + state.getCards(Location.DRAW_PILE, CardInstance.Type.ANY));

        System.out.println (color + "Player Dice:");
        for (Dice dice : state.getActionDice()) {
            System.out.print(color + "\t" + state.getValueOf(dice));
        }
        System.out.println();

        System.out.println(color + "Player Money: " +
                state.getPlayerMoney(state.getCurrentPlayer()) + " Imperial Septims");
        System.out.println("Player Victory Points: " + state.getPlayerPoints(state.getCurrentPlayer()));

        System.out.println();

        System.out.println("You have " + state.getCards(Location.PLAYER_HAND, CardType.ANY).size() + " cards");

        for (DiceDisc d : state.getDiceDiscs()) {
            String s;
            for (CardInstance c : state.getCardsOnDisc(d, state.getCurrentPlayer())) {
                System.out.print(String.format("%15s", c.getName()));
            }
            if (state.getCardsOnDisc(d, state.getCurrentPlayer()).isEmpty()) {
                System.out.print(String.format("%15s", ""));
            }

            if (state.getDiscIntegerValue(d) != Constants.BRIBE_DISC_INT) {
                System.out.print(" <DICE DISC " + state.getDiscIntegerValue(d) + "> ");
            } else {
                System.out.print(" <BRIBE DISC> ");
            }

            for (Player p : state.getOpponents(state.getCurrentPlayer())) {
                for (CardInstance c : state.getCardsOnDisc(d, p)) {
                    System.out.print(c.getName());
                }
            }
//            s = String.format("%30s : %-30s", state.getCardsOnDisc(d, state.getCurrentPlayer()).iterator().next(), state.getCardsOnDisc(d, state.getOpponents(state.getCurrentPlayer()).iterator().next()));

//            System.out.println(s);





            if (state.getIsBlocked(d, state.getCurrentPlayer(), state)) {
                System.out.print("<Activation Blocked>");
            }
//
//            if (!state.getCardsOnDisc(d, state.getCurrentPlayer()).isEmpty()) {
//                System.out.println(state.getCardsOnDisc(d, state.getCurrentPlayer()).iterator().next());
//
//            } else {

                System.out.println();
//            }
        }

        //System.out.print("\u001B[0m");

        // TODO FIX THIS SHIT

        if (state.isComplete()) {
            System.out.println("Game finished!");
        }
    }

    public static void presentToUser(List<Choice> intents) {
        int choiceNum = FIRST_CHOICE;

        System.out.println("There are " + intents.size() +
                " available choices for you: ");

        for (Choice i : intents) {
            System.out.println(choiceNum++ + ") " + i.getDescription());
        }
    }
}
