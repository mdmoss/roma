/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matt
 */
public class BlockingInterface implements ChoiceHandler {

    List<Choice> choices;
    Choice chosen;
    LocalGameState gameState;
    boolean ready;

    public BlockingInterface(LocalGameState gameState) {
        this.gameState = gameState;
    }

    public void makeChoice (Choice chosen) {
        this.chosen = chosen;
    }

    List<Choice> getChoices() {
        return new LinkedList<Choice>(choices);
    }

    @Override
    public void dealWithChoices(List<Choice> choices) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void report(String report) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
