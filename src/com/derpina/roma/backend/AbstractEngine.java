/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.cards.ActionRewindTime;
import com.derpina.roma.core.Choice;
import java.util.*;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public abstract class AbstractEngine {
    private final ChoiceHandler ui;

    public AbstractEngine(ChoiceHandler ui) {
        this.ui = ui;
    }

    protected void execute(List<Action> frame) {
        boolean timeTravelling = false;
        Action timeTraveller = null;

        for (Action action : frame) {
            if (action.getClass() == ActionRewindTime.class) {
                timeTraveller = action;
                timeTravelling = true;
                break;
            }
            assert (action.isValid());

            Collection<Choice> choices = action.getIntents();
            while (!choices.isEmpty()) {
                ui.dealWithChoices((List<Choice>) choices);
                choices = action.getIntents();
            }

            action.execute();
            ui.report(action.getReport());
            actionHooks(action);

            List<Action> children = new LinkedList<Action>(action.next());
            execute(children);
        }

        if (timeTravelling) {
            actionHooks(timeTraveller);
        }
    }

    protected void actionHooks(Action action, Action parent) {
    }

    protected void actionHooks(Action action) {
    }
}
