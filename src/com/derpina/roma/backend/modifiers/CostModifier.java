/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.modifiers;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public interface CostModifier extends TemporaryModifier {

    int modifyCost (LocalGameState gameState, int original, CardInstance card);
    CostModifier clone();
}
