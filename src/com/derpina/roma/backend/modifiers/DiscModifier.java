/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.modifiers;

import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Player;

/**
 *
 * @author Matt
 */
public interface DiscModifier extends TemporaryModifier {

    boolean isBlocked (LocalGameState gameState, boolean previous, DiceDisc disc, Player who);
    DiscModifier clone();
}
