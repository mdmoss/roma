/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.modifiers;

import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public interface TemporaryModifier {

    void mutate(LocalGameState gameState);
}
