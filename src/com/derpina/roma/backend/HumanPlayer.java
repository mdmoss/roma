/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.core.*;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;


/**
 *
 * @author Matt
 */
public class HumanPlayer implements Player {

    private String name;
    private Color color;

    public HumanPlayer(int id, String name) {
        this.name = name;
        this.color = Color.RED;

    }

    public HumanPlayer(int id, String name, Color color) {
        this.name = name;
        this.color = color;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Color getColor() {
        return color;
    }
}
