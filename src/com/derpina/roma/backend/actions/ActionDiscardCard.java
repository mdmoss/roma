/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.actions.literal.ActionLiteralPutCardInDiscardPile;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionDiscardCard extends RealAction {

    CardInstance toDiscard;

    public ActionDiscardCard(LocalGameState gameState, CardInstance toDiscard) {
        super(gameState);
        this.toDiscard = toDiscard;
    }

    @Override
    public boolean isValid() {
        return gameState.getCardLocation(toDiscard) != null;
    }

    @Override
    public void execute() {
        this.forceNextAction(toDiscard.getDiscardAction(gameState, toDiscard));
    }
}
