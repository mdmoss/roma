/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.actions;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Player;
import com.derpina.roma.core.Rules;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class ActionStartGame extends RealAction {

    public ActionStartGame(LocalGameState gameState) {
        super(gameState);

        addNextAction(new ActionDrawCards(
                gameState, Rules.STARTING_CARDS * Rules.NUM_PLAYERS));

        for (Player p : gameState.getPlayers()) {
            addNextAction(new ActionDealCards(gameState, p, Rules.STARTING_CARDS));
        }
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {

    }
}
