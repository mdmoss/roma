/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.ArrayList;
import java.util.Collection;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.backend.choosers.DiceDiscFactory;
import com.derpina.roma.backend.choosers.PlaceableCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Matt
 */
public class ActionPlaceCard extends RealAction {
    private PlaceableCardFactory cardToPlace;
    private DiceDiscFactory disc;

    public ActionPlaceCard(LocalGameState gameState) {
        super(gameState);
        cardToPlace = new PlaceableCardFactory(gameState);
        this.registerChoice(cardToPlace);
        disc = new DiceDiscFactory(gameState);
        this.registerChoice(disc);
    }

    @Override
    public boolean isValid() {
        for (CardInstance card : gameState.getCards(Location.PLAYER_HAND, CardType.ANY)) {
            if (gameState.getPlayerMoney(gameState.getCurrentPlayer()) >= gameState.getCostFor(card, gameState)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void execute() {
        Collection<CardInstance> current = gameState.getCardsOnDisc(disc.getValue(), gameState.getCurrentPlayer());
        for (CardInstance c : current) {
            this.addNextAction(c.getDiscardAction(gameState, c));
        }

        this.addNextAction(new ActionPayPlaceCost(gameState, cardToPlace.getValue()));
        this.addNextAction(new ActionMoveCardToDisc(gameState, cardToPlace.getValue(), disc.getValue()));
        this.addNextAction(cardToPlace.getValue().getPlaceAction(gameState, disc.getValue(), cardToPlace.getValue()));
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice (ChoiceType.PLACE_CARD, "Place a card", this, owner);
    }
}