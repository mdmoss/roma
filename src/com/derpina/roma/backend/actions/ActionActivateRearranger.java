/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.cards.ActivateAction;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Matt
 */
public class ActionActivateRearranger extends ActivateAction {
    private final CardType type;

    public ActionActivateRearranger(LocalGameState gameState, CardInstance card, CardType type) {
        super(gameState, card);
        this.type = type;
    }

    @Override
    public void execute() {
        super.execute();

        for (CardInstance laid : gameState.getCards(Location.DICE_DISC, type)) {
            gameState.makeCardFloating(laid);
        }
    }

    @Override
    public List<Action> next() {
        List<Action> actions = new ArrayList<Action>();
        if (!gameState.getCards(Location.FLOATING, CardType.ANY).isEmpty()) {
            actions.add(new ActionLayFloatingCard(gameState));
        } else {
            actions.addAll(super.next());
        }

        return actions;
    }
}