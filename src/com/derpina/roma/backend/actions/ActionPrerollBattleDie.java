/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.DiceRoller;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionPrerollBattleDie extends RealAction {

    private final int battleDiceRoll;

    public ActionPrerollBattleDie(LocalGameState gameState) {
        super(gameState);
        battleDiceRoll = DiceRoller.rollDice();
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        gameState.setNextBattleDie(battleDiceRoll);
    }
}
