/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.Arrays;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionStartChain extends MetaAction {

    public ActionStartChain(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    protected Iterable<Action> getChildren() {

        Action[] possibleNext = new Action[] {

            new ActionActivateMoneyDisc(gameState),
            new ActionActivateCardsDisc(gameState),
            new ActionPlaceCard(gameState),
            new ActionActivateCard(gameState),
            new ActionActivateBribeDisc(gameState),
            new ActionEndTurn(gameState)
        };

        return Arrays.asList(possibleNext);
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        assert (false);
        return new Choice(ChoiceType.TRUE_FALSE, "You should never see this", this, owner);
    }
}
