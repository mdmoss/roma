/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.cards.ActivateAction;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.backend.choosers.ArbitraryCardFactory;
import com.derpina.roma.core.*;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author Matt
 */
public class ActionActivateCard extends RealAction {

    ArbitraryCardFactory card;

    public ActionActivateCard(LocalGameState gameState) {
        super(gameState);
        card = new ArbitraryCardFactory(gameState, getValidActivateCards());
        this.registerChoice(card);
    }

    @Override
    public boolean isValid() {
        return !getValidActivateCards().isEmpty();
    }

    @Override
    public void execute() {
        if (!gameState.getIsBlocked(gameState.getCardLocation(card.getValue()), gameState.getCurrentPlayer(), gameState)) {
            gameState.useUpDice(gameState.getActionDiceOfValue(gameState.getCardLocation(card.getValue())));
            this.addNextAction(card.getValue().getActivateAction(gameState, card.getValue()));
            this.executeReport = gameState.getCurrentPlayer().toString() + " activated their " + card.getValue() + " on Dice Disc " + gameState.getCardLocation(card.getValue());
        }
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice (ChoiceType.ACTIVATE_CARD, "Activate a card", this, owner);
    }

    private Collection<CardInstance> getValidActivateCards() {

        Collection<CardInstance> validCards = new LinkedList<CardInstance>();

        for (CardInstance check : gameState.getCards(Location.DICE_DISC, CardType.ANY)) {
            DiceDisc loc = gameState.getCardLocation(check);
            if (gameState.getActionDiceOfValue(loc) != null &&
                    gameState.getDiscIntegerValue(loc) != Constants.BRIBE_DISC_INT &&
                    gameState.getActionDice().size() > check.getNumDiceRequired()) {

                ActivateAction a = (ActivateAction) check.getActivateAction(gameState, check);
                a.setActivatingCard(check);

                // Pretend the dice disc is used
                Dice hide = gameState.getActionDiceOfValue(loc);
                gameState.maskDice(hide);
                if (a.isValid()) {
                    validCards.add(check);
                }
                gameState.revealDice(hide);

            }
        }

        return validCards;
    }
}
