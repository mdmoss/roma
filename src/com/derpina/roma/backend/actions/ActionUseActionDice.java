/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.Dice;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionUseActionDice extends RealAction {

    Dice dice;

    public ActionUseActionDice(LocalGameState gameState, Dice dice) {
        super(gameState);
        this.dice = dice;
    }

    @Override
    public boolean isValid() {
        return !gameState.getActionDice().isEmpty();
    }

    @Override
    public void execute() {
        gameState.useUpDice(dice);
    }
}
