/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.CardFactory;
import com.derpina.roma.backend.choosers.DiceDiscFactory;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionLayFloatingCard extends RealAction {

    private DiceDiscFactory disc;
    private CardFactory card;

    public ActionLayFloatingCard(LocalGameState gameState) {
        super(gameState);

        card = new CardFactory(gameState, Location.FLOATING, CardType.ANY);
        this.registerChoice(card);
        disc = new DiceDiscFactory(gameState);
        this.registerChoice(disc);
    }

    @Override
    public boolean isValid() {
        return !gameState.getCards(Location.FLOATING, CardType.ANY).isEmpty();
    }

    @Override
    public void execute() {


        this.forceNextAction(new ActionMoveCardToDisc(gameState, card.getValue(), disc.getValue()));

        if (gameState.getCards(Location.FLOATING, CardType.ANY).size() > 1) {
            this.addNextAction(new ActionLayFloatingCard(gameState));
        }
    }
}
