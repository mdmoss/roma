/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.actions.literal.ActionLiteralPutCardOnDisc;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionMoveCardToDisc extends RealAction {
    private final CardInstance gameCard;
    private final DiceDisc diceDisc;

    public ActionMoveCardToDisc(LocalGameState gameState, CardInstance gameCard, DiceDisc diceDisc) {
        super(gameState);
        this.gameCard = gameCard;
        this.diceDisc = diceDisc;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        for (CardInstance c : gameState.getCardsOnDisc(diceDisc, gameState.getCurrentPlayer())) {
            this.addNextAction(new ActionDiscardCard(gameState, c));
        }
        this.addNextAction(new ActionLiteralPutCardOnDisc(gameState, gameCard, diceDisc));
    }
}
