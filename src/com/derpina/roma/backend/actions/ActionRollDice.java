/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.DiceRoller;
import com.derpina.roma.core.Dice;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Rules;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionRollDice extends RealAction {

    Collection<Integer> diceValues;

    public ActionRollDice(LocalGameState gameState) {
        super(gameState);

        diceValues = new ArrayList<Integer>();
        for (int i = 0; i < Rules.TURN_ACTION_DICE; i++) {
            diceValues.add(DiceRoller.rollDice());
        }
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        assert (diceValues.size() == Rules.TURN_ACTION_DICE);
        gameState.setActionDiceValues(diceValues);
    }
}
