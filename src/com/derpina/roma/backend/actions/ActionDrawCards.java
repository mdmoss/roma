/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.actions;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class ActionDrawCards extends RealAction {
    private int numToDraw;

    public ActionDrawCards(LocalGameState gameState, int numToDraw) {
        super(gameState);
        this.numToDraw = numToDraw;
    }

    private void drawCards(int number) {

        List<CardInstance> drawPile = (List<CardInstance>) gameState.getCards(Location.DRAW_PILE, CardType.ANY);

        if (drawPile.size() < number) {
            List<CardInstance> discardPile = (List<CardInstance>) gameState.getCards(Location.DISCARD_PILE, CardType.ANY);

            Random rand = new Random();
            while (discardPile.size() > 0) {

                CardInstance toMove = discardPile.get(
                        rand.nextInt(discardPile.size()));
                gameState.moveCardToDrawPile(toMove);

                discardPile = (List<CardInstance>)
                        gameState.getCards(Location.DISCARD_PILE, CardType.ANY);
            }
        }
        drawPile = (List<CardInstance>) gameState.getCards(Location.DRAW_PILE, CardType.ANY);
        for (int i = 0; i < number && i < drawPile.size(); i++) {
            gameState.makeCardFloating(drawPile.get(i));
        }
        assert (!gameState.getCards(Location.FLOATING, CardType.ANY).isEmpty());
    }

    @Override
    public boolean isValid() {
        int drawableCards =
                gameState.getCards(
                    Location.DISCARD_PILE,
                    CardType.ANY).size()
                          +
                gameState.getCards(
                    Location.DRAW_PILE,
                    CardType.ANY).size();

        return drawableCards > 0;
    }

    @Override
    public void execute() {
        drawCards(numToDraw);
    }

    void setNumCardsToDraw(int num) {
        this.numToDraw = num;
    }
}
