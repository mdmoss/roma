/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.Collection;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionActivateMoneyDisc extends RealAction {

    ActionDiceFactory dice;

    public ActionActivateMoneyDisc(LocalGameState gameState) {
        super(gameState);
        dice = new ActionDiceFactory(gameState);
        this.registerChoice(dice);
    }

    @Override
    public boolean isValid() {
        return !(gameState.getActionDice().isEmpty());
    }

    @Override
    public void execute() {
        gameState.earnPlayerMoney(gameState.getCurrentPlayer(), gameState.getValueOf(dice.getValue()));
        gameState.useUpDice(dice.getValue());
        executeReport = gameState.getCurrentPlayer() + " activated the Money Disc for " + gameState.getValueOf(dice.getValue()) + " sestetii";
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice(ChoiceType.ACTIVATE_MONEY_DISC, "Activate Money Disk", this, owner);
    }
}
