/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.DiceRoller;
import com.derpina.roma.core.Dice;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionCardAttack extends RealAction {

    CardInstance target;

    public ActionCardAttack(CardInstance target, LocalGameState gameState) {
        super(gameState);
        this.target = target;
        this.addNextAction(new ActionPrerollBattleDie(gameState));
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        assert (target != null);
        Dice attackRoll = gameState.getBattleDie();
        if (gameState.getValueOf(attackRoll) >= gameState.getDefenceFor(target, gameState)) {
            this.forceNextAction(new ActionDiscardCard(gameState, target));
            executeReport = gameState.getCurrentPlayer() + " attacked the opponent's " + target.getName() + ", and it was defeated!";
        } else {
            executeReport = gameState.getCurrentPlayer() + " attacked the opponent's " + target.getName() + ", but was not succesful";
        }
    }
}
