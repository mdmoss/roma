/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.cards.Mercator;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.ArbitraryActionDiceFactory;
import com.derpina.roma.core.*;
import java.util.Collection;
import java.util.LinkedList;

/**
 *
 * @author Matt
 */
public class ActionActivateBribeDisc extends RealAction {

    ArbitraryActionDiceFactory actionDiceFactory;

    public ActionActivateBribeDisc(LocalGameState gameState) {
        super(gameState);

        Collection<Dice> validDice = new LinkedList<Dice>();

        for (Dice dice : gameState.getActionDice()) {
            if (gameState.getValueOf(dice) <= gameState.getPlayerMoney(gameState.getCurrentPlayer())) {
                validDice.add(dice);
            }
        }

        actionDiceFactory = new ArbitraryActionDiceFactory(gameState, validDice);
        this.registerChoice(actionDiceFactory);
    }

    @Override
    public boolean isValid() {

        CardInstance card = null;
        Collection<CardInstance> cardsOnDisc = gameState.getCardsOnDisc(gameState.getBribeDisc(), gameState.getCurrentPlayer());
        if (!cardsOnDisc.isEmpty()) {
            card = cardsOnDisc.iterator().next();
        }

        boolean hasDiceAndMoney = false;
        for (Dice dice : gameState.getActionDice()) {
            if (gameState.getValueOf(dice) <= gameState.getPlayerMoney(gameState.getCurrentPlayer()) &&
                    card != null &&
                    (gameState.getActionDice().size() >= card.getNumDiceRequired() + 1)) {
                hasDiceAndMoney = true;
                break;
            }
        }

        boolean cardWouldBeValidToActivate = false;
        if (hasDiceAndMoney) {
            Collection <Dice> currentDice = new LinkedList <Dice> (gameState.getActionDice());
            // Pretend the dice disc is used
            Dice hide = gameState.getActionDice().iterator().next();
            gameState.maskDice(hide);
            cardWouldBeValidToActivate = card.getActivateAction(gameState, card).isValid();
            gameState.revealDice(hide);
        }


        return hasDiceAndMoney && cardWouldBeValidToActivate;
    }

    @Override
    public void execute() {
        if (!gameState.getIsBlocked(gameState.getBribeDisc(), gameState.getCurrentPlayer(), gameState)) {
            gameState.useUpDice(actionDiceFactory.getValue());
            gameState.spendPlayerMoney(gameState.getCurrentPlayer(), gameState.getValueOf(actionDiceFactory.getValue()));
            CardInstance toExec = gameState.getCardsOnDisc(gameState.getBribeDisc(), gameState.getCurrentPlayer()).iterator().next();
            this.addNextAction(toExec.getActivateAction(gameState, toExec));
            executeReport = gameState.getCurrentPlayer().toString() + " activated the Bribe Disc";
        }
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice(ChoiceType.ACTIVATE_BRIBE_DISC, "Activate card on Bribe Disc", this, owner);
    }


}
