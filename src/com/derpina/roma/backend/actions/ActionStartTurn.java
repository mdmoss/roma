/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import com.derpina.roma.core.Rules;

/**
 *
 * @author Matt
 */
public class ActionStartTurn extends RealAction {

    public ActionStartTurn(LocalGameState gameState) {
        super(gameState);
        this.addNextAction(new ActionRollDice(gameState));
        this.addNextAction(new ActionPrerollBattleDie(gameState));
        this.addNextAction(new ActionPlaceTimeWarpedCards(gameState));
        this.addNextAction(new ActionDealWithTime(gameState));
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        int cardsOnField = gameState.getCards(Location.DICE_DISC, CardType.ANY).size();
        int cost = Rules.NUM_CARD_FIELDS - cardsOnField;
        gameState.movePointsToPool(gameState.getCurrentPlayer(), cost);
    }
}
