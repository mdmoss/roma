/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.choosers.DiceDiscFactory;
import com.derpina.roma.backend.choosers.HandCardOfTypeFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;

/**
 *
 * @author Matt
 */
public class ActionFreePlaceType extends RealAction {
    private final CardType type;

    private HandCardOfTypeFactory card;
    private DiceDiscFactory disc;

    public ActionFreePlaceType(LocalGameState gameState, CardType type) {
        super(gameState);

        this.type = type;

        card = new HandCardOfTypeFactory(gameState, type);
        this.registerChoice(card);
        disc = new DiceDiscFactory(gameState);
        this.registerChoice(disc);
    }

    @Override
    public void execute() {
        this.forceNextAction(new ActionMoveCardToDisc(gameState, card.getValue(), disc.getValue()));
        this.addNextAction(new ActionContinueFreePlace(gameState, type));
        executeReport = gameState.getCurrentPlayer() + " placed a " + card.getValue().getName() + " for free";
    }

    @Override
    public boolean isValid() {
        return !gameState.getCards(Location.PLAYER_HAND, type).isEmpty();
    }
}
