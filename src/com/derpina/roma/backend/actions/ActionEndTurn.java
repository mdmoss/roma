/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.Collection;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.backend.modifiers.TemporaryModifier;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.Dice;
import com.derpina.roma.utils.InstantCollection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionEndTurn extends RealAction {

    public ActionEndTurn(LocalGameState gameState) {
        super(gameState);
        this.addNextAction(new ActionStartTurn(gameState));
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        gameState.incrementTurn();
        Collection<TemporaryModifier> mods = gameState.getAllModifiers();
        for (TemporaryModifier mod : mods) {
            mod.mutate(gameState);
        }
        gameState.useUpRemainingDice();
        executeReport = gameState.getCurrentPlayer() + " ended their turn";
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice(ChoiceType.END_TURN, "End Turn", this, owner);
    }
}
