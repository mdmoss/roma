/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.ArrayList;
import java.util.Collection;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.Choice;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public abstract class RealAction extends AbstractAction {



    public RealAction(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    public abstract boolean isValid();

    @Override
    public abstract void execute();

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        assert (false);
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Collection<Choice> getIntents() {
        for (AbstractChoiceFactory factory : choiceProviders) {
            if (!factory.isValueSet()) {
                return factory.getChoices();
            }
        }
        return new ArrayList<Choice>();
    }

    List<AbstractChoiceFactory> choiceProviders = new LinkedList<AbstractChoiceFactory>();

    public void registerChoice (AbstractChoiceFactory factory) {
        choiceProviders.add(factory);
    }
}
