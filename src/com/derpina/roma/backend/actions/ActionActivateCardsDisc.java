/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.CardFactory;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.*;


/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ActionActivateCardsDisc extends RealAction {

    ActionDiceFactory actionDie;

    ActionDrawCards cardDrawer;
    ActionKeepCard cardKeeper;

    public ActionActivateCardsDisc(LocalGameState gameState) {
        super(gameState);
        actionDie = new ActionDiceFactory(gameState);
        this.registerChoice(actionDie);

        cardDrawer = new ActionDrawCards(gameState, 0);
        addNextAction(cardDrawer);
    }

    @Override
    public boolean isValid() {
        return (!gameState.getActionDice().isEmpty()) && cardDrawer.isValid();
    }

    @Override
    public void execute() {

        cardDrawer.setNumCardsToDraw(gameState.getValueOf(actionDie.getValue()));
        gameState.useUpDice(actionDie.getValue());
        cardKeeper = new ActionKeepCard(gameState);
        addNextAction(cardKeeper);
        executeReport = gameState.getCurrentPlayer() + " activated the Cards Disc";
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice(ChoiceType.ACTIVATE_CARD_DISC, "Activate Card Disc", this, owner);
    }
}

class ActionKeepCard extends RealAction {

    CardFactory cardChoice;

    public ActionKeepCard(LocalGameState gameState) {
        super(gameState);
        cardChoice = new CardFactory(gameState, Location.FLOATING, CardType.ANY);
        this.registerChoice(cardChoice);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        assert (!gameState.getCards(Location.FLOATING, CardType.ANY).isEmpty());
        gameState.moveCardToHand(cardChoice.getValue(), gameState.getCurrentPlayer());
        executeReport = gameState.getCurrentPlayer() + " chose a card";
        for (CardInstance card : gameState.getCards(Location.FLOATING, CardType.ANY)) {
            gameState.moveCardToDiscardPile(card);
        }
        gameState.moveCardToHand(cardChoice.getValue(), gameState.getCurrentPlayer());

    }
}