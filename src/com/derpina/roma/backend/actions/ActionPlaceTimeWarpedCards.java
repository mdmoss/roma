/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Player;

/**
 *
 * @author Matt
 */
public class ActionPlaceTimeWarpedCards extends RealAction {

    public ActionPlaceTimeWarpedCards(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        for (DiceDisc d : gameState.getDiceDiscs()) {
            for (Player p : gameState.getPlayers()) {
                CardInstance card = gameState.getPendingCardPlacements(gameState, p, d, gameState.getCurrentTurnNo());
                if (card != null) {
                    for (CardInstance c : gameState.getCardsOnDisc(d, p)) {
                        this.addNextAction(c.getDiscardAction(gameState, c));
                    }
                    gameState.moveCardToDisc(card, d, p);
                }
            }
        }
    }
}
