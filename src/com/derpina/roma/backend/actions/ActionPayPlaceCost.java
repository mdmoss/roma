/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionPayPlaceCost extends RealAction {

    CardInstance gameCard;

    public ActionPayPlaceCost(LocalGameState gameState, CardInstance gameCard) {
        super(gameState);
        this.gameCard = gameCard;
    }

    @Override
    public boolean isValid() {
        return gameState.getPlayerMoney(gameState.getCurrentPlayer()) >= gameState.getCostFor(gameCard, gameState);
    }

    @Override
    public void execute() {
        gameState.spendPlayerMoney(gameState.getCurrentPlayer(), gameState.getCostFor(gameCard, gameState));
    }
}
