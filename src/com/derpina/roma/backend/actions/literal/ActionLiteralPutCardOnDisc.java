/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions.literal;

import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionLiteralPutCardOnDisc extends RealAction {

    private final CardInstance gameCard;
    private final DiceDisc diceDisc;

    public ActionLiteralPutCardOnDisc(LocalGameState gameState, CardInstance gameCard, DiceDisc diceDisc) {
        super(gameState);
        this.gameCard = gameCard;
        this.diceDisc = diceDisc;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        gameState.moveCardToDisc(gameCard, diceDisc);
        executeReport = gameState.getCurrentPlayer() + " laid their " +gameCard.getName() + " on Dice Disc " + gameState.getDiscIntegerValue(diceDisc);
    }
}
