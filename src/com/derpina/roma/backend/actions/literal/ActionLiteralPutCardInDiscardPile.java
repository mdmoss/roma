/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions.literal;

import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.cards.GrimReaper;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;

/**
 *
 * @author Matt
 */
public class ActionLiteralPutCardInDiscardPile extends RealAction {

    private final CardInstance gameCard;

    public ActionLiteralPutCardInDiscardPile(LocalGameState gameState, CardInstance gameCard) {
        super(gameState);
        this.gameCard = gameCard;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        boolean grimReaperActive = false;
        if (gameState.getOwner(gameCard) != gameState.getCurrentPlayer()) {
            for (CardInstance card : gameState.getOpposingCards(Location.DICE_DISC, CardType.CHARACTER)) {
                if (card.getGameCard().getClass() == GrimReaper.class &&
                        gameCard.getGameCard().getClass() != GrimReaper.class) {
                    grimReaperActive = true;
                }
            }
        }

        if (grimReaperActive) {
            gameState.moveCardToHand(gameCard, gameState.getOwner(gameCard));
            executeReport = gameCard.getName() + "was saved by the Grim Reaper";
        } else {
            gameState.moveCardToDiscardPile(gameCard);
            executeReport = gameCard.getName() + "was discarded";
        }
    }
}
