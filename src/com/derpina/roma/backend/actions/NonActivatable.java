/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.cards.ActivateAction;
import com.derpina.roma.core.CardInstance;

/**
 *
 * @author Matt
 */
public class NonActivatable extends ActivateAction {

    public NonActivatable(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public boolean isValid() {
        return false;
    }
}
