/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.Collection;

import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import java.util.List;

/**
 *
 * @author Matt
 * @author Lasath
 */
public interface Action {

    // core
    public boolean isValid();

    abstract public void execute();

    // top level action extends core
    public Choice registerAction(AbstractChoiceFactory owner);

    // should this be in core?
    public List<Action> next();

    // interactiveAction extends core
    public Collection<Choice> getIntents();

    public void changeAffectedState(LocalGameState gameState);

    public String getReport();
}
