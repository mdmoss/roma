/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.core.LocalGameState;
import framework.interfaces.GameState;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matt
 */
public abstract class AbstractAction implements Action {

    private List<Action> futures;
    protected LocalGameState gameState;
    protected String executeReport = "";

    public AbstractAction(LocalGameState gameState) {
        futures = new LinkedList<Action>();
        this.gameState = gameState;
    }

    @Override
    public List<Action> next() {
        return futures;
    }

    public void addNextAction (Action action) {
        futures.add(action);
    }

    public void forceNextAction (Action action) {
        futures.add(0, action);
    }

    @Override
    public void changeAffectedState(LocalGameState gameState) {
        this.gameState = gameState;
    }

    public LocalGameState getAffectedState () {
        return this.gameState;
    }

    @Override
    public String getReport() {
        return executeReport;
    }
}
