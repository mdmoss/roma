/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import java.util.Collection;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.choosers.ActionChoiceFactory;
import com.derpina.roma.core.Choice;
import com.derpina.roma.utils.InstantCollection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public abstract class MetaAction extends AbstractAction {

    private ActionChoiceFactory children;

    public MetaAction(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    public boolean isValid() {
        for (Action child : getChildren()) {
            if (child.isValid()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void execute() {
        this.addNextAction(children.getValue());
    }

    @Override
    public Collection<Choice> getIntents() {
        if (children == null) {
            children = new ActionChoiceFactory(gameState, getChildren());
        }
        return children.getChoices();
    }

    protected abstract Iterable<Action> getChildren();
}