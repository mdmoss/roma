/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.cards.ActionRewindTime;
import com.derpina.roma.backend.cards.ActivateAction;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.ArbitraryCardFactory;
import com.derpina.roma.backend.choosers.BooleanChoiceFactory;
import com.derpina.roma.core.*;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class ActionActivateTelephoneBox extends ActivateAction {

    BooleanChoiceFactory travelForward;
    ArbitraryCardFactory chosenCard;
    ActionDiceFactory chosenDice;

    public ActionActivateTelephoneBox(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        travelForward = new BooleanChoiceFactory(gameState, "Travel Forward", "Travel Backward and Break All the Things");
        this.registerChoice(travelForward);
        Collection<CardInstance> possibilities = gameState.getCards(Location.DICE_DISC, CardType.ANY);
        possibilities.remove(card);
        chosenCard = new ArbitraryCardFactory(gameState, possibilities);
        this.registerChoice(chosenCard);

        this.chosenDice = new ActionDiceFactory(gameState);
        this.registerChoice(chosenDice);
    }

    @Override
    public boolean isValid() {
        return gameState.getCards(Location.DICE_DISC, CardType.ANY).size() > 1;
    }

    @Override
    public void execute() {
        if (travelForward.getValue()) {
            this.addNextAction(new ActionActivateForwardTravel(chosenCard.getValue(), chosenDice.getValue(), gameState));
        } else {
            this.addNextAction(new ActionRewindTime(chosenCard.getValue(), chosenDice.getValue(), gameState));
        }
    }
}

class ActionActivateForwardTravel extends RealAction {

    CardInstance target;
    Dice activateValue;

    public ActionActivateForwardTravel(CardInstance target, Dice activateValue, LocalGameState gameState) {
        super(gameState);
        this.target = target;
        this.activateValue = activateValue;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        gameState.useUpDice(activateValue);
        gameState.schedule(gameState.getCurrentPlayer(), target, gameState.getCardLocation(target), gameState.getCurrentTurnNo() + gameState.getValueOf(activateValue));
        gameState.moveCardToTimeWarp(target);
    }
}
