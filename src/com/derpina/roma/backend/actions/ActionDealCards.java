/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import com.derpina.roma.core.Player;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class ActionDealCards extends RealAction {
    private final Player player;
    private final int numCards;

    public ActionDealCards(LocalGameState gameState, Player player, int numCards) {
        super(gameState);
        this.player = player;
        this.numCards = numCards;
    }

    @Override
    public boolean isValid() {
        return gameState.getCards(
                Location.FLOATING,
                CardType.ANY).size() >= this.numCards;
    }

    @Override
    public void execute() {
        List<CardInstance> cards = (List<CardInstance>) gameState.getCards(
                Location.FLOATING, CardType.ANY);

        for (int i = 0; i < numCards; i++) {
            gameState.moveCardToHand(cards.get(i), player);
        }
    }

}
