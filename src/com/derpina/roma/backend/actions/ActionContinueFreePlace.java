/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.actions;

import com.derpina.roma.backend.choosers.BooleanChoiceFactory;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;
import com.derpina.roma.utils.InstantCollection;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Matt
 */
public class ActionContinueFreePlace extends RealAction {
    private final CardType type;
    BooleanChoiceFactory decision;

    public ActionContinueFreePlace(LocalGameState gameState, CardType type) {
        super(gameState);
        this.type = type;
        decision = new BooleanChoiceFactory(gameState, "Yes", "No");
        this.registerChoice(decision);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        if (decision.getValue() == true && new ActionFreePlaceType(gameState, type).isValid()) {
            this.addNextAction(new ActionFreePlaceType(gameState, type));
        } else {
            executeReport = gameState.getCurrentPlayer() + " finished placing cards";
        }
    }
}
