/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionDealWithTime;
import com.derpina.roma.backend.actions.ActionPrerollBattleDie;
import com.derpina.roma.backend.actions.ActionStartChain;
import com.derpina.roma.backend.cards.ActionRewindTime;
import com.derpina.roma.core.LocalGameState;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matt
 */
public class TimeEngine extends AbstractEngine {
    private LocalGameState activeState;
    private final LinkedList<List<Action>> actionHistory;
    private final LinkedList<LocalGameState> stateHistory;
    private LinkedList<Action> turnActions;

    public TimeEngine(ChoiceHandler ui, LocalGameState initial) {
        super(ui);
        this.activeState = initial;
        this.actionHistory = new LinkedList<List<Action>>();
        this.stateHistory = new LinkedList<LocalGameState>();
        this.turnActions = new LinkedList<Action>();
    }

    public void run() {
        startChain(new ActionPrerollBattleDie(getActiveState()));
        while (!activeState.isComplete()) {
            startChain(new ActionStartChain(getActiveState()));
        }
    }

    protected void startChain(Action chainStart) {

        List newActions = new LinkedList<Action>();
        newActions.add(chainStart);
        execute(newActions);
    }

    public LocalGameState getActiveState() {
        return activeState;
    }

    public void setActiveState(LocalGameState activeState) {
        this.activeState = activeState;
    }

    @Override
    protected void actionHooks(Action action) {
        super.actionHooks(action);
        turnActions.add(action);

        if (action.getClass() == ActionDealWithTime.class) {
            this.stateHistory.add(getActiveState());
            setActiveState(getActiveState().clone());

            this.actionHistory.add(turnActions);
            this.turnActions = new LinkedList<Action>();
        } else if (action.getClass() == ActionRewindTime.class) {


            setActiveState(rewindTime((ActionRewindTime)action, getActiveState(), actionHistory, stateHistory, turnActions));
        }
    }

    private LocalGameState rewindTime (ActionRewindTime cause, LocalGameState changed, List<List<Action>> actionHistory, List<LocalGameState> stateHistory, List<Action> turnActions) {

        // Get a GameState to rebuild from
        LocalGameState historyState = stateHistory.get(stateHistory.size() - cause.getRequiredTimeTravel());

        // We've got a previous state
        // First, make the time travel change
        historyState.addCardToGame(cause.getTarget());
        historyState.moveCardToDisc(cause.getTarget(), cause.getLandingZone());

        // We've got a changed previous state, fantastic. Let's roll out our changes

        for (int i = changed.getCurrentTurnNo() - cause.getRequiredTimeTravel(); i < changed.getCurrentTurnNo(); i++) {

            for (Action a : actionHistory.get(i)) {
                a.changeAffectedState(historyState);
                a.execute();
            }
            stateHistory.set(i, historyState.clone());
        }

        // We're back where we were when we started this turn. Better redo those actions as well

        for (Action a : turnActions) {
            a.changeAffectedState(historyState);
            a.execute();
        }

        // And we're done
        return historyState;
    }
}
