/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.ui.console.Printer;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class HumanInterface implements ChoiceHandler {
    private final LocalGameState state;

    public HumanInterface(LocalGameState current) {
        this.state = current;
    }

    @Override
    public void dealWithChoices(List<Choice> choices) {
        //Printer.showGameInfo(state);
        Printer.presentToUser((List<Choice>) choices);

        int choice = 0;
        do {
            System.out.print("Please enter a choice (" +
                                Printer.FIRST_CHOICE + ".." + choices.size() + ") :");
            try {
                choice = Integer.parseInt(System.console().readLine());
            } catch (NumberFormatException e) {
                System.out.println("That is not a number!");
            } catch (java.lang.NullPointerException e) {
                System.out.println("This UI must be run in a standards "
                        + "compliant console. Sorry for the inconvenience.");
                System.exit(1);
            }
        } while (choice > choices.size() || choice < Printer.FIRST_CHOICE);

        choices.get(choice - 1).getOwner().chosen(choices.get(choice - 1));
    }

    public static void main(String args[]) {
        LocalGameState gameState = new AdvancedGameState(new RomaBuilder());
        ChoiceHandler handler = new HumanInterface(gameState);
        TimeEngine engine = new TimeEngine(handler, gameState);

        engine.run();
    }

    @Override
    public void report(String report) {
        if (!report.equals("")) {
            System.out.println(report);
        }
    }

}
