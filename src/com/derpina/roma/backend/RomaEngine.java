/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.*;
import com.derpina.roma.backend.cards.ActionRewindTime;
import com.derpina.roma.backend.gamestate.GameStateAdaptor;
import com.derpina.roma.core.*;
import com.derpina.roma.ui.console.Printer;
import com.derpina.roma.utils.InstantCollection;
import framework.interfaces.GameState;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class RomaEngine extends AbstractEngine {

    List<LocalGameState> stateHistory;
    List<List<Action>> actionHistory;
    List<Action> turnActions;
    private LocalGameState activeState;

    public void setActiveState(LocalGameState activeState) {
        this.activeState = activeState;
    }

    public RomaEngine(ChoiceHandler ui, LocalGameState initial) {
        super(ui);
        this.activeState = initial;
        this.actionHistory = new ArrayList<List<Action>>();
        this.stateHistory = new ArrayList<LocalGameState>();
        this.turnActions = new ArrayList<Action>();
    }

    public void run() {
        startChain(new ActionPrerollBattleDie(getActiveState()));
        while (!activeState.isComplete()) {
            startChain(new ActionStartChain(getActiveState()));
        }
    }

    protected void startChain(Action chainStart) {
        //Printer.showGameInfo(getActiveState());
        execute(InstantCollection.get(chainStart));
    }

    @Override
    protected void actionHooks(Action action) {
        super.actionHooks(action);
        turnActions.add(action);

        assert (((AbstractAction)action).getAffectedState() == getActiveState());

        if (action.getClass() == ActionDealWithTime.class) {

            LocalGameState original = getActiveState();
            this.stateHistory.add(getActiveState());
            setActiveState(getActiveState().clone());
            LocalGameState clone = getActiveState();

            assert(checkEquals(original, clone));

            this.actionHistory.add(turnActions);
            this.turnActions = new ArrayList<Action>();
            if (this.actionHistory.size() > Rules.TARDUS_MAX_RANGE) {
                this.actionHistory.remove(0);
            }
        } else if (action.getClass() == ActionRewindTime.class) {
            ActionRewindTime tardis = (ActionRewindTime) action;
            int numTurns = tardis.getRequiredTimeTravel();
            CardInstance passenger = tardis.getTarget();
            DiceDisc landingZone = getActiveState().getCardLocation(passenger);
            Player owner = getActiveState().getOwner(passenger);

            this.setActiveState(this.stateHistory.get(stateHistory.size() - numTurns));
            this.getActiveState().addCardToGame(passenger);
            this.getActiveState().moveCardToDisc(passenger, landingZone, owner);


            for (int i = actionHistory.size() - numTurns; i < actionHistory.size(); i++) {

                for (Action a : actionHistory.get(i)) {
                    a.changeAffectedState(getActiveState());
                    a.execute();
                }
            }
            for (Action a : turnActions) {
                if (a.getClass() != ActionActivateTelephoneBox.class) {
                    a.changeAffectedState(getActiveState());
                    a.execute();
                } else {
                    break;
                }
            }
        }
    }

    public LocalGameState getActiveState() {
        return activeState;
    }

        public static boolean checkEquals (LocalGameState originalState, LocalGameState cloneState) {

        GameState original = new GameStateAdaptor(originalState);
        GameState clone = new GameStateAdaptor(cloneState);

        //assert (Arrays.equals(Arrays.asLoriginal.getActionDice(), clone.getActionDice()));
        assert (original.getDeck().equals(clone.getDeck()));
        assert (original.getDiscard().equals(clone.getDiscard()));
        assert Arrays.equals(original.getPlayerCardsOnDiscs(0), clone.getPlayerCardsOnDiscs(0));
        assert Arrays.equals(original.getPlayerCardsOnDiscs(1), clone.getPlayerCardsOnDiscs(1));
        assert (original.getPlayerHand(0).equals(clone.getPlayerHand(0)));
        assert (original.getPlayerHand(1).equals(clone.getPlayerHand(1)));
        assert (original.getPlayerSestertii(0) == clone.getPlayerSestertii(0));
        assert (original.getPlayerSestertii(1) == clone.getPlayerSestertii(1));
        assert (original.getPlayerVictoryPoints(0) == clone.getPlayerVictoryPoints(0));
        assert (original.getPlayerVictoryPoints(1) == clone.getPlayerVictoryPoints(1));
        assert (original.getPoolVictoryPoints() == clone.getPoolVictoryPoints());
        assert (original.getWhoseTurn() == clone.getWhoseTurn());

        return true;
    }
}
