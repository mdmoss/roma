/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionDealWithTime;
import com.derpina.roma.backend.actions.ActionPrerollBattleDie;
import com.derpina.roma.backend.actions.ActionStartChain;
import com.derpina.roma.backend.cards.ActionRewindTime;
import com.derpina.roma.core.LocalGameState;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matt
 */
public class TimelessEngine extends AbstractEngine {
    private LocalGameState activeState;

    public TimelessEngine(ChoiceHandler ui, LocalGameState initial) {
        super(ui);
        this.activeState = initial;
    }

    public void run() {
        startChain(new ActionPrerollBattleDie(getActiveState()));
        while (!activeState.isComplete()) {
            startChain(new ActionStartChain(getActiveState()));
        }
    }

    protected void startChain(Action chainStart) {

        List newActions = new LinkedList<Action>();
        newActions.add(chainStart);
        execute(newActions);
    }

    public LocalGameState getActiveState() {
        return activeState;
    }

    public void setActiveState(LocalGameState activeState) {
        this.activeState = activeState;
    }
}
