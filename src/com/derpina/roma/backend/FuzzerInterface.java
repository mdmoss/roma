/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.ui.console.Printer;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class FuzzerInterface implements ChoiceHandler {
    final public static int NUM_GAMES = 1000;

    Random rand = new Random();
    LocalGameState state;

    public FuzzerInterface(LocalGameState state) {
        this.state = state;
    }

    @Override
    public void dealWithChoices(List<Choice> choices) {
        Printer.presentToUser(choices);

        int choiceNum = rand.nextInt(choices.size());
        //System.out.println("Chosen " + choices.get(choiceNum).getDescription());
        choices.get(choiceNum).getOwner().chosen(choices.get(choiceNum));
    }

    public static void main(String[] args) {
        for (int i = 0; i < NUM_GAMES; i++) {
            LocalGameState gameState = new AdvancedGameState(new RomaBuilder());
            TimeEngine engine = new TimeEngine(new FuzzerInterface(gameState), gameState);

            engine.run();

            System.out.println ("****************************************************************");
            System.out.println ("turn No :" + gameState.getCurrentTurnNo());
            System.out.println ("Finished game " + i);
            System.out.println (gameState.getCurrentPlayer().getName() + " was victorius!");
            System.out.println ("****************************************************************");
        }
    }

    @Override
    public void report(String report) {
        if (!report.equals("")) {
            System.out.println(report);
        }
    }
}
