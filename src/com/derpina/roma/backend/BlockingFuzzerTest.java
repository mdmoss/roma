/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.ui.console.Printer;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class BlockingFuzzerTest  {
    final public static int NUM_GAMES = 100;

    public BlockingInterface control;

    Random rand = new Random();
    LocalGameState state;

    public BlockingFuzzerTest() {

        LocalGameState gameState = new AdvancedGameState(new RomaBuilder());
        BlockingInterface control = new BlockingInterface(gameState);
        TimeEngine engine = new TimeEngine(control, gameState);

        while (gameState.isComplete()) {
            List<Choice> choices = control.getChoices();
            int choiceNum = rand.nextInt(choices.size());
            System.out.println("Choosing " + choices.get(choiceNum).toString());
            control.makeChoice(choices.get(choiceNum));
        }
        System.out.println ("Finished game");
        System.out.println ("****************************************************************");
    }

}
