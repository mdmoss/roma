/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.core.Choice;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public interface ChoiceHandler {

    public void dealWithChoices(List<Choice> choices);
    public void report (String report);
}
