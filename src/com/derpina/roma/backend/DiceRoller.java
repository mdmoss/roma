/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.RomaDice;
import java.util.Random;

import com.derpina.roma.core.Dice;
import com.derpina.roma.core.Rules;

/**
 *
 * @author Matt
 */
public class DiceRoller {

    public static int rollDice() {
        return (new Random().nextInt(Rules.NUM_DICE_SIDES) + 1);
    }

    public static int rollDice(int numSides) {
        return (new Random().nextInt(numSides) + 1);
    }
}