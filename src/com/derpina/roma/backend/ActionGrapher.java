/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionStartChain;
import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import com.derpina.roma.core.LocalGameState;
import javax.swing.JFrame;
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultEdge;
import org.jgrapht.ListenableGraph;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.ListenableUndirectedGraph;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class ActionGrapher extends RomaEngine {

    public final static int HORIZONTAL_INCREMENT = 50;
    public final static int VERTICAL_INCREMENT = 20;

    JFrame m_frame;
    ListenableGraph m_graph;

    public ActionGrapher(ChoiceHandler ui, LocalGameState initial) {
        super(ui, initial);

        m_graph = new ListenableUndirectedGraph(DefaultEdge.class);
        JGraphModelAdapter adapter = new JGraphModelAdapter(m_graph);
        JGraph graph = new JGraph(adapter);

        m_frame = new JFrame("Roma Actions Graph");
        m_frame.getContentPane().add(graph);

        m_frame.setSize(600, 400);
        m_frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        m_frame.setVisible(true);
    }

    Action currentParent;
    int horizontalDepth = 0;
    int verticalDepth = 0;

    @Override
    protected void actionHooks(Action action, Action parent) {
        if (!m_graph.containsVertex(parent)) {
            m_graph.addVertex(parent);
        }

        m_graph.addVertex(action);
        System.out.println("adding vertex : " + action);
        m_graph.addEdge(parent, action);

        super.actionHooks(action, parent);
    }

    public static void main(String[] args) {
        LocalGameState state = new AdvancedGameState(new RomaBuilder());
        ActionGrapher ag = new ActionGrapher(new FuzzerInterface(state), state);

        ag.run();
    }
}
