/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import java.util.Collection;

/**
 * @author Lasath Fernando
 * @author Matt
 */
public class ActionChoiceFactory extends AbstractChoiceFactory<Action> {

    private Iterable<Action> actions;

    public ActionChoiceFactory(LocalGameState gameState, Iterable<Action> actions) {
        super(gameState);
        this.actions = actions;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (Action action : actions) {
            if (action.isValid()) {
                choices.add(action.registerAction(this));
            }
        }
    }
}
