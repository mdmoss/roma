/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import java.util.Collection;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.Dice;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class ActionDiceFactory extends AbstractChoiceFactory<Dice> {

    public ActionDiceFactory(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    public void populateChoices(Collection<Choice> choices) {
        for (Dice dice : gameState.getActionDice()) {
            choices.add(new Choice(ChoiceType.CHOOSE_ACTION_DICE,
                    "Choose Dice of value " + gameState.getValueOf(dice), dice, this));
        }
    }
}
