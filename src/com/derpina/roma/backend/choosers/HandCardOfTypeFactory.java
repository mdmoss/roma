/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando
 * @author Mattew Moss
 */
public class HandCardOfTypeFactory extends AbstractChoiceFactory<CardInstance> {

    CardType type;

    public HandCardOfTypeFactory(LocalGameState gameState, CardType type) {
        super(gameState);
        this.type = type;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : gameState.getCards(Location.PLAYER_HAND, type)) {
            choices.add(new Choice(
                    ChoiceType.CHOOSE_CARD,
                    "Place a " + card.getName(),
                    card,
                    this));
        }
    }
}
