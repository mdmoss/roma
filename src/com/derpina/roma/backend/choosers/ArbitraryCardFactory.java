/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import java.util.Collection;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class ArbitraryCardFactory extends AbstractChoiceFactory<CardInstance> {

    Collection<CardInstance> cards;

    public ArbitraryCardFactory(LocalGameState gameState,
            Collection<CardInstance> cards) {
        super(gameState);
        this.cards = cards;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : cards) {
            choices.add(new Choice(
                    ChoiceType.CHOOSE_CARD,
                    "Choose a " + card.getName(),
                    card,
                    this));
        }
    }

}
