/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import java.util.ArrayList;
import java.util.Collection;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public abstract class AbstractChoiceFactory<ValueType> {

    protected ValueType chosen;
    protected LocalGameState gameState;

    protected String description = "";

    public AbstractChoiceFactory(LocalGameState gameState) {
        this.gameState = gameState;
    }

    public boolean isValueSet() {
        return chosen != null;
    }

    public ValueType getValue() {
        assert (chosen != null);
        return chosen;
    }

    abstract protected void populateChoices(Collection<Choice> choices);

    public Collection<Choice> getChoices() {
        Collection<Choice> current = new ArrayList<Choice>();

        if (!isValueSet()) {
            populateChoices(current);
        }

        return current;
    }

    @SuppressWarnings("unchecked")
    public void chosen(Choice choice) {

        ValueType value = (ValueType) choice.getValue();
        this.chosen = value;

    }

    public void setDescription (String description) {
        this.description = description;
    }

    public String getDescription () {
        return this.description;
    }
}
