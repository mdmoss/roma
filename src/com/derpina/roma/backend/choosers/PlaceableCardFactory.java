/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class PlaceableCardFactory extends AbstractChoiceFactory<CardInstance> {

    public PlaceableCardFactory(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : gameState.getCards(
                Location.PLAYER_HAND, CardType.ANY)) {
            if (gameState.getPlayerMoney(gameState.getCurrentPlayer()) >= gameState.getCostFor(card, gameState)) {
                choices.add(new Choice(
                        ChoiceType.CHOOSE_CARD,
                        "Place a " + card.getName(),
                        card,
                        this));
            }
        }
    }

}
