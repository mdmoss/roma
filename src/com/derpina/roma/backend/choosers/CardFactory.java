/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.*;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class CardFactory extends AbstractChoiceFactory<CardInstance> {
    private final Location location;
    private final CardType type;

    public CardFactory(LocalGameState gameState, Location location, CardType type) {
        super(gameState);
        this.location = location;
        this.type = type;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : gameState.getCards(location, type)) {
            choices.add(new Choice(ChoiceType.CHOOSE_CARD, "Choose A " + card.getName(), card, this));
        }
    }
}
