/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import java.util.Collection;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss <mdm@cse.unsw.edu.au>
 */
public class OpposingLaidCardFactory extends AbstractChoiceFactory<CardInstance> {

    CardType type;

    public OpposingLaidCardFactory(LocalGameState gameState, CardType type) {
        super(gameState);
        this.type = type;
    }

    @Override
    public void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : gameState.getOpposingCards(
                Location.DICE_DISC,
                type)) {

            choices.add(new Choice(
                    ChoiceType.CHOOSE_CARD,
                    "Choose opponent's " + card.getName(),
                    card,
                    this));
        }
    }

}
