/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.LocalGameState;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class BooleanChoiceFactory extends AbstractChoiceFactory<Boolean> {
    private final String truePrompt;
    private final String falsePrompt;

    public BooleanChoiceFactory(
            LocalGameState gameState,
            String truePrompt,
            String falsePrompt) {
        super(gameState);

        this.truePrompt = truePrompt;
        this.falsePrompt = falsePrompt;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        choices.add(new Choice(ChoiceType.TRUE_FALSE, truePrompt, true, this));
        choices.add(new Choice(ChoiceType.TRUE_FALSE, falsePrompt, false, this));
    }
}
