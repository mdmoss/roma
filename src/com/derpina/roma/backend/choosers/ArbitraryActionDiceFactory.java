/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.Dice;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.LocalGameState;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class ArbitraryActionDiceFactory extends AbstractChoiceFactory<Dice> {
    private final Collection<Dice> validDice;

    public ArbitraryActionDiceFactory(LocalGameState gameState, Collection<Dice> validDice) {
        super(gameState);
        this.validDice = validDice;
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (Dice dice : validDice) {
            choices.add(new Choice(ChoiceType.CHOOSE_ACTION_DICE, "Choose Action dice of value " + gameState.getValueOf(dice), dice, this));
        }
    }


}
