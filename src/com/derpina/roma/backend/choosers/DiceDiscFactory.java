/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.*;
import java.util.Collection;


/**
 *
 * @author Matt
 */
public class DiceDiscFactory extends AbstractChoiceFactory<DiceDisc> {

    public DiceDiscFactory(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (DiceDisc disc : gameState.getDiceDiscs()) {
            if (gameState.getDiscIntegerValue(disc) != Constants.BRIBE_DISC_INT) {
                choices.add(new Choice(
                        ChoiceType.CHOOSE_DICE_DISC,
                        "Choose dice disc " + gameState.getDiscIntegerValue(disc),
                        disc,
                        this));
            } else {
                                choices.add(new Choice(
                        ChoiceType.CHOOSE_DICE_DISC,
                        "Choose the bribe disc",
                        disc,
                        this));
            }
        }
    }
}
