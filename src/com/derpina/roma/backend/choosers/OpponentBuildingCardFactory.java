/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.choosers;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class OpponentBuildingCardFactory extends AbstractChoiceFactory<CardInstance> {

    @Deprecated
    public OpponentBuildingCardFactory(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        for (CardInstance card : gameState.getOpposingCards(
                Location.DICE_DISC, CardType.BUILDING)) {
            choices.add(new Choice(
                    ChoiceType.CHOOSE_CARD,
                    "Choose opponent's " + card.getName(),
                    card,
                    this));
        }
    }
}
