/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import java.io.*;
import java.net.InetSocketAddress;
import java.util.List;
import org.w3c.dom.Element;

/**
 *
 * @author Matt
 */
public class WebInterface {

    HttpServer server;
    TimelessEngine gameEngine;
    GameInputHandler inputHandler;

    public WebInterface() {

        try {
            System.out.println("foo");

            inputHandler = new GameInputHandler();
            LocalGameState gameState = new AdvancedGameState(new RomaBuilder());
            gameEngine = new TimelessEngine(inputHandler, gameState);
            inputHandler.setEngine(gameEngine);

            this.server = HttpServer.create(new InetSocketAddress(2911), 256);
            server.createContext("/", inputHandler);
            server.start();

        } catch (IOException ex) {
            System.err.println("An error occured starting the Roma server. We apologise for the inconveniece");
            System.exit(1);
        }
    }

    public static void main (String[] args) {

        WebInterface game = new WebInterface();
    }
}

class GameInputHandler implements HttpHandler, ChoiceHandler {

    TimelessEngine engine;

    public void setEngine (TimelessEngine engine) {
        this.engine = engine;
    }

    @Override
    public void handle(HttpExchange he) throws IOException {

        System.out.println(he.getRequestBody());

        String resp = renderGameState(null);
        he.sendResponseHeaders(200, 0);
        OutputStream response = he.getResponseBody();

        PrintWriter writer = new PrintWriter(response);
        writer.println("Hello I am a happy computer");
        writer.close();
    }

    @Override
    public void dealWithChoices(List<Choice> choices) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void report(String report) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String renderGameState (LocalGameState gameState) {
        return "derp";
    }
}
