/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.Dice;
import com.derpina.roma.core.DiceManager;
import com.derpina.roma.core.ObjectBuilder;
import com.derpina.roma.core.Rules;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author Matt
 */
final public class RomaDiceManager implements DiceManager {

    Dice battleDie;
    HashMap<Dice, Integer> diceValues;
    LinkedList<Dice> hiddenDice;
    LinkedList<Dice> usedDice;

    public RomaDiceManager(ObjectBuilder builder) {

        diceValues = new HashMap<Dice, Integer>();
        for (int i = 0; i < Rules.TURN_ACTION_DICE; i++) {
            diceValues.put(builder.createDice(), i);
        }

        battleDie = builder.createDice();
        diceValues.put(battleDie, new Random().nextInt(Rules.NUM_DICE_SIDES) + 1);

        hiddenDice = new LinkedList<Dice>();
        usedDice = new LinkedList<Dice>();

        useUpRemainingDice();
    }

    RomaDiceManager(RomaDiceManager romaDiceManager) {
        this.diceValues = new HashMap<Dice, Integer>();
        for (Dice d : romaDiceManager.diceValues.keySet()) {
            this.diceValues.put(d, romaDiceManager.diceValues.get(d).intValue());
        }

        this.battleDie = romaDiceManager.battleDie;
        this.hiddenDice = new LinkedList<Dice>(romaDiceManager.hiddenDice);
        this.usedDice = new LinkedList<Dice>(romaDiceManager.usedDice);
    }

    @Override
    public Collection<Dice> getActionDice() {

        Collection<Dice> result = new LinkedList<Dice>(diceValues.keySet());
        result.removeAll(hiddenDice);
        result.removeAll(usedDice);
        result.remove(battleDie);
        return result;
    }
    @Override
    public void useUpDice(Dice value) {
        usedDice.add(value);
    }

    @Override
    public int getValueOf(Dice dice) {
        return diceValues.get(dice);
    }

    @Override
    public void setNextBattleDie(int roll) {
        diceValues.put(battleDie, roll);
    }

    @Override
    public Dice getBattleDie() {
        return battleDie;
    }

    @Override
    public void setValueOfDice(Dice dice, int value) {
        diceValues.put(dice, value);
    }

    @Override
    public void maskDice(Dice dice) {
        hiddenDice.add(dice);
    }

    @Override
    public void revealDice(Dice dice) {
        hiddenDice.remove(dice);
    }

    @Override
    public void unmaskAllDice() {
        hiddenDice.clear();
    }

    @Override
    public Dice getDiceOfValue(int roll) {
        for (Dice d : diceValues.keySet()) {
            if (!hiddenDice.contains(d) && !usedDice.contains(d) && d != (battleDie) && diceValues.get(d) == roll) {
                return d;
            }
        }
        return null;
    }

    @Override
    public void useUpRemainingDice() {
        hiddenDice.clear();
        usedDice.clear();

        for (Dice dice : diceValues.keySet()) {
            if (dice != battleDie) {
                usedDice.add(dice);
            }
        }
    }

    @Override
    public void setActionDiceValues(Collection<Integer> values) {
        useUpRemainingDice();

        for (Integer v : values) {
            diceValues.put(usedDice.remove(0), v);
        }
    }
}
