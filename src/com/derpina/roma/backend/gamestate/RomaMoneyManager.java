/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.MoneyManager;
import com.derpina.roma.core.Player;
import com.derpina.roma.core.PlayerManager;
import com.derpina.roma.core.Rules;
import java.util.HashMap;

/**
 *
 * @author Matt
 */
public class RomaMoneyManager implements MoneyManager {

    HashMap<Player, Integer> money;

    public RomaMoneyManager(PlayerManager playerManager) {
        this.money = new HashMap<Player, Integer>();

        for (Player p : playerManager.getPlayers()) {
            money.put(p, Rules.STARTING_MONEY);
        }
    }

    RomaMoneyManager(RomaMoneyManager romaMoneyManager, PlayerManager playerManager) {

        this.money = new HashMap<Player, Integer>();

        for (Player p : romaMoneyManager.money.keySet()) {
            money.put(p, romaMoneyManager.money.get(p).intValue());
        }
    }

    @Override
    public void earnPlayerMoney(Player player, int amount) {
        int newMoney = money.get(player) + amount;
        money.put(player, newMoney);
    }

    @Override
    public void spendPlayerMoney(Player player, int amount) {
        int newMoney = money.get(player) - amount;
        money.put(player, newMoney);
    }

    @Override
    public int getPlayerMoney(Player player) {
        return money.get(player);
    }
}
