/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.GamePhaseManager;
import com.derpina.roma.core.Player;
import com.derpina.roma.core.PlayerManager;
import com.derpina.roma.core.VictoryPointManager;

/**
 *
 * @author Matt
 */
public class RomaGamePhaseManager implements GamePhaseManager {
    private final PlayerManager playerManager;
    private final VictoryPointManager victoryPointManager;

    public RomaGamePhaseManager(PlayerManager playerManager, VictoryPointManager victoryPointManager) {
        this.playerManager = playerManager;
        this.victoryPointManager = victoryPointManager;
    }

    RomaGamePhaseManager(RomaGamePhaseManager romaGamePhaseManager, PlayerManager playerManager, VictoryPointManager victoryPointManager) {
        this.playerManager = playerManager;
        this.victoryPointManager = victoryPointManager;
    }

    @Override
    public boolean isComplete() {
        if (victoryPointManager.getPool() == 0) {
            return true;
        }
        for (Player p : playerManager.getPlayers()) {
            if (victoryPointManager.getPlayerPoints(p) == 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Player getWinner() {
        int winningPoints = -1;
        Player winningPlayer = null;
        for (Player p : playerManager.getPlayers()) {
            if (victoryPointManager.getPlayerPoints(p) > winningPoints) {
                winningPoints = victoryPointManager.getPlayerPoints(p);
                winningPlayer = p;
            }
        }
        return winningPlayer;
    }
}
