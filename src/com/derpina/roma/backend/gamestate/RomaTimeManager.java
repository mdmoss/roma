/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.*;
import framework.interfaces.GameState;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;


/**
 *
 * @author Matt
 */
public class RomaTimeManager implements TimeManager {

    List<PlaceCardEvent> timeEvents = new LinkedList<PlaceCardEvent>();

    public RomaTimeManager() {
        this.timeEvents = new LinkedList<PlaceCardEvent>();
    }

    RomaTimeManager(RomaTimeManager romaTimeManager) {
        this.timeEvents = new LinkedList<PlaceCardEvent> (romaTimeManager.timeEvents);
    }

    @Override
    public void schedule(Player player, CardInstance card, DiceDisc dest, int turn) {
        timeEvents.add(new PlaceCardEvent(player, card.getGameCard(), dest, turn));
    }

    @Override
    public CardInstance getPendingCardPlacements(LocalGameState gameState, Player player, DiceDisc loc, int turn) {

        PlaceCardEvent occured = null;
        CardInstance toReturn = null;

        for (PlaceCardEvent event : timeEvents) {
            if (event.turn == turn && event.disc == loc && event.player == player) {
                for (CardInstance card : gameState.getCards(Location.TIME_WARP, CardType.ANY)) {
                    if (card.getGameCard() == (event.card)) {
                        occured = event;
                        toReturn = card;
                        break;
                    }
                }
            }
        }
        if (occured != null) {
            timeEvents.remove(occured);
        }
        return toReturn;
    }
}
class PlaceCardEvent {

    public Player player;
    public GameCard card;
    public DiceDisc disc;
    public int turn;

    public PlaceCardEvent(Player player, GameCard card, DiceDisc disc, int turn) {
        this.card = card;
        this.disc = disc;
        this.turn = turn;
        this.player = player;
    }
}