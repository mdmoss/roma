/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.*;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Matt
 */
class RomaVictoryPointManager implements VictoryPointManager {
    private int victoryPointPool = Rules.TOTAL_VICTORY_POINTS;
    private Map<Player, Integer> points = new HashMap<Player, Integer>();

    RomaVictoryPointManager(PlayerManager manager) {
        for (Player p : manager.getPlayers()) {
            points.put(p, Rules.STARTING_VICTORY_POINTS);
            victoryPointPool -= Rules.STARTING_VICTORY_POINTS;
        }
    }

    RomaVictoryPointManager(RomaVictoryPointManager romaVictoryPointManager, PlayerManager playerManager) {
        this.victoryPointPool = romaVictoryPointManager.victoryPointPool;
        this.points = new HashMap<Player, Integer>();
        for (Player p : romaVictoryPointManager.points.keySet()) {
            points.put(p, romaVictoryPointManager.points.get(p).intValue());
        }
    }

    @Override
    public int getPool() {
        return victoryPointPool;
    }

    @Override
    public void setPool(int numPoints) {
        this.victoryPointPool = numPoints;
    }

    @Override
    public int getPlayerPoints (Player p) {

        return (int) points.get(p);
    }

    @Override
    public void setPlayerPoints (Player p, int value) {

        points.put(p, value);
    }

    private void changePlayerPoints (Player p, int amount) {

        int current = getPlayerPoints(p);
        setPlayerPoints(p, current + amount);
    }

    @Override
    public void moveVictoryPoints(Player from, Player to, int amount) {

        if (getPlayerPoints(from) >= amount) {
            changePlayerPoints(from, (-amount));
            changePlayerPoints(to, amount);
        } else {
            int validPoints = getPlayerPoints(from);
            changePlayerPoints(from, (-validPoints));
            changePlayerPoints(to, amount);
        }
    }

    @Override
    public void movePointsToPool(Player from, int amount) {
        if (getPlayerPoints(from) >= amount) {
            changePlayerPoints(from, (-amount));
            setPool(getPool() + amount);
        } else {
            int validPoints = getPlayerPoints(from);
            changePlayerPoints(from, (-validPoints));
            setPool(getPool() + validPoints);
        }
    }

    @Override
    public void movePointsFromPool(Player to, int amount) {
        if (getPool() >= amount) {
            setPool(getPool() - amount);
            changePlayerPoints(to, amount);
        } else {
            int validPoints = getPool();
            setPool(getPool() - validPoints);
            changePlayerPoints(to, validPoints);
        }
    }
}
