/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.backend.cards.Turris;
import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.backend.modifiers.DefenceModifier;
import com.derpina.roma.backend.modifiers.DiscModifier;
import com.derpina.roma.backend.modifiers.TemporaryModifier;
import com.derpina.roma.core.*;
import java.util.*;

/**
 *
 * @author Matt
 */
public class RomaModificationManager implements ObjectPropertiesManager {

    List<CostModifier> costPipeline = new LinkedList<CostModifier>();
    List<DiscModifier> discPipeline = new LinkedList<DiscModifier>();
    List<DefenceModifier> defencePipeline = new LinkedList<DefenceModifier>();

    GameCardManager gameCardManager;

    public RomaModificationManager(GameCardManager gameCardManager) {
        this.gameCardManager = gameCardManager;
    }

    RomaModificationManager(RomaModificationManager romaModificationManager, GameCardManager gameCardManager) {
        this.gameCardManager = gameCardManager;

        this.costPipeline = new LinkedList<CostModifier>(romaModificationManager.costPipeline);
        this.discPipeline = new LinkedList<DiscModifier>(romaModificationManager.discPipeline);
        this.defencePipeline = new LinkedList<DefenceModifier>(romaModificationManager.defencePipeline);
    }

    @Override
    public int getCostFor(CardInstance card, LocalGameState gameState) {
        int currentCost = card.getBaseCost();
        for (CostModifier modifier : costPipeline) {
            currentCost = modifier.modifyCost(gameState, currentCost, card);
        }
        return currentCost;
    }

    @Override
    public int getDefenceFor(CardInstance card, LocalGameState gameState) {
        int currentDefence = card.getBaseDefense();
        for (DefenceModifier modifier : defencePipeline) {
            currentDefence = modifier.modifyDefence(gameState, currentDefence, card);
        }
        if (gameCardManager.getOwner(card) != null) {
            for (CardInstance c : gameCardManager.getCards(Location.DICE_DISC, CardType.ANY, gameCardManager.getOwner(card))) {
                if (c.getGameCard().getClass() == Turris.class && c != card) {
                    currentDefence += Rules.TURRIS_DEFENCE_INCREASE;
                }
            }
        }
        return currentDefence;
    }

    @Override
    public boolean getIsBlocked(DiceDisc disc, Player asking, LocalGameState gameState) {
        boolean currentResult = false;
        for (DiscModifier modifier : discPipeline) {
            currentResult = modifier.isBlocked(gameState, currentResult, disc, asking);
        }
        return currentResult;
    }

    @Override
    public List<CostModifier> getCostModifiers() {
        return costPipeline;
    }

    @Override
    public List<DefenceModifier> getDefenceModifiers() {
        return defencePipeline;
    }

    @Override
    public List<DiscModifier> getDiscModifiers() {
        return discPipeline;
    }

    @Override
    public void setCostModifiers(List<CostModifier> modifiers) {
        this.costPipeline = modifiers;
    }

    @Override
    public void setDefenceModifiers(List<DefenceModifier> modifiers) {
        this.defencePipeline = modifiers;
    }

    @Override
    public void setDiscModifiers(List<DiscModifier> modifiers) {
        this.discPipeline = modifiers;
    }

    @Override
    public Collection<TemporaryModifier> getAllModifiers() {
        Collection<TemporaryModifier> mods = new LinkedList<TemporaryModifier>();
        mods.addAll(costPipeline);
        mods.addAll(defencePipeline);
        mods.addAll(discPipeline);
        return mods;
    }
}
