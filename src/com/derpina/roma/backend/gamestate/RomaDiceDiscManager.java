/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.DiceDiscManager;
import com.derpina.roma.core.ObjectBuilder;
import com.derpina.roma.core.Rules;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Matt
 */
public class RomaDiceDiscManager implements DiceDiscManager {

    List<DiceDisc> diceDiscs;

    public RomaDiceDiscManager(ObjectBuilder builder) {
        diceDiscs = new ArrayList<DiceDisc>();
        for (int i = 0; i < Rules.NUM_CARD_FIELDS; i++) {
            diceDiscs.add(builder.createDiceDisc());
        }
    }

    RomaDiceDiscManager(RomaDiceDiscManager romaDiceDiscManager) {
        this.diceDiscs = new ArrayList<DiceDisc>(romaDiceDiscManager.diceDiscs);
    }

    @Override
    public List<DiceDisc> getDiceDiscs() {
        return Collections.unmodifiableList(diceDiscs);
    }
}
