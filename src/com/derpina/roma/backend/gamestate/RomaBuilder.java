/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.backend.HumanPlayer;
import com.derpina.roma.backend.RomaDiceDisc;
import com.derpina.roma.backend.cards.*;
import com.derpina.roma.core.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class RomaBuilder implements ObjectBuilder {

    final HashMap<Class<? extends GameCard>, Integer> initialDeckNumbers = new HashMap<Class<? extends GameCard>, Integer>();
    final Collection<CardInstance> initialDeck = new ArrayList<CardInstance>();

    @Override
    public GameCardManager createCardManager(PlayerManager playerManager, DiceDiscManager diceDiscManager, DiceAssociationManager diceAssociationManager) {
        return new RomaGameCardManager(this, playerManager, diceDiscManager, diceAssociationManager);
    }

    @Override
    public TurnManager createTurnManager() {
        return new RomaTurnManager();
    }

    @Override
    public Collection<CardInstance> getInitialDrawPile() {
        initialDeck.clear();

        for (Class<? extends GameCard> c : initialDeckNumbers.keySet()) {
            try {
                for (int i = 0; i < initialDeckNumbers.get(c); i++) {
                    GameCard card = c.newInstance();
                    initialDeck.add(card.createInstance());
                }
            } catch (InstantiationException ex) {
                Logger.getLogger(RomaBuilder.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(1);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(RomaBuilder.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(1);
            }
        }

        return initialDeck;
    }

    private String[] namesArray;

    public RomaBuilder() {
        this.namesArray = new String[]{
            "Augustus",
            "Tiberius",
            "Caligula",
            "Claudius",
            "Commodus",
            "Brutus",
            "Julius"
        };

        this.initialDeckNumbers.put(Aesculapinum.class,   2);
        this.initialDeckNumbers.put(Architectus.class,    2);
        this.initialDeckNumbers.put(Basilica.class,       2);
        this.initialDeckNumbers.put(Centurio.class,       2);
        this.initialDeckNumbers.put(Consiliarius.class,   2);
        this.initialDeckNumbers.put(Consul.class,         2);
        this.initialDeckNumbers.put(Essedum.class,        2);
        this.initialDeckNumbers.put(Forum.class,          2);
        this.initialDeckNumbers.put(Gladiator.class,      2);
        this.initialDeckNumbers.put(GrimReaper.class,     2);
        this.initialDeckNumbers.put(Haruspex.class,       2);
        this.initialDeckNumbers.put(Kat.class,            2);
        this.initialDeckNumbers.put(Legat.class,          2);
        this.initialDeckNumbers.put(Legionarius.class,    2);
        this.initialDeckNumbers.put(Machina.class,        2);
        this.initialDeckNumbers.put(Mercator.class,       2);
        this.initialDeckNumbers.put(Mercatus.class,       2);
        this.initialDeckNumbers.put(Nero.class,           2);
        this.initialDeckNumbers.put(Onager.class,         2);
        this.initialDeckNumbers.put(Praetorianus.class,   2);
        this.initialDeckNumbers.put(Scaenicus.class,      2);
        this.initialDeckNumbers.put(Senetor.class,        2);
        this.initialDeckNumbers.put(Sicarius.class,       2);
        this.initialDeckNumbers.put(TelephoneBox.class,   2);
        this.initialDeckNumbers.put(Templum.class,        2);
        this.initialDeckNumbers.put(TribunusPlebis.class, 2);
        this.initialDeckNumbers.put(Turris.class,         2);
        this.initialDeckNumbers.put(Velites.class,        2);
    }

    private String generateName() {

        Random random = new Random();
        String name = "Lumpy";

        final ArrayList<String> names = new ArrayList<String>();

        for (String newName : this.namesArray) {
            names.add(newName + " Caesar");
        }

        for (int i = 0; i < names.size(); i++) {
            int nameToUse = random.nextInt((names.size() + 1) * 10);
            if (nameToUse == (names.size() * 10)) {
                name = "Caesar Salad";
            } else {
                name = names.get(nameToUse % names.size());
                names.remove(nameToUse % names.size());
            }
        }
        return name;
    }

    @Override
    public Player createPlayer() {
        return new HumanPlayer(generateName().charAt(0), generateName());
    }

    @Override
    public VictoryPointManager createVictoryPointManager(PlayerManager p) {
        return new RomaVictoryPointManager(p);
    }

    @Override
    public DiceDiscManager createDiceDiscManager() {
        return new RomaDiceDiscManager(this);
    }

    @Override
    public DiceDisc createDiceDisc() {
        return new RomaDiceDisc();
    }

    @Override
    public DiceManager createActionDiceManager() {
        return new RomaDiceManager(this);
    }

    @Override
    public MoneyManager createMoneyManager(PlayerManager p) {
        return new RomaMoneyManager(p);
    }

    @Override
    public GamePhaseManager createGamePhaseManager(PlayerManager p, VictoryPointManager vpm) {
        return new RomaGamePhaseManager(p, vpm);
    }

    @Override
    public PlayerManager createPlayerManager(TurnManager tm) {
        return new RomaPlayerManager(this, tm);
    }

    @Override
    public DiceAssociationManager createDiceAssociationManager(DiceDiscManager ddm, DiceManager adm) {
        return new RomaDiceAssociationManager(this, ddm, adm);
    }

    @Override
    public ObjectPropertiesManager createObjectPropertiesManager (GameCardManager gameCardManager) {
        return new RomaModificationManager(gameCardManager);
    }

    @Override
    public TimeManager createTimeManager() {
        return new RomaTimeManager();
    }

    @Override
    public Dice createDice() {
        return new RomaDice();
    }
}