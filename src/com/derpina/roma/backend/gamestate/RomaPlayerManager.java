/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.ObjectBuilder;
import com.derpina.roma.core.Player;
import com.derpina.roma.core.PlayerManager;
import com.derpina.roma.core.Rules;
import com.derpina.roma.core.TurnManager;
import java.util.*;

/**
 *
 * @author Matt
 */
class RomaPlayerManager implements PlayerManager {

    private List<Player> players;
    private TurnManager turnManager;

    RomaPlayerManager(ObjectBuilder builder, TurnManager tm) {
        players = new ArrayList<Player>(Rules.NUM_PLAYERS);

        for (int i = 0; i < Rules.NUM_PLAYERS; i++) {
            players.add(builder.createPlayer());
        }

        this.turnManager = tm;
    }

    RomaPlayerManager(RomaPlayerManager romaPlayerManager, TurnManager manager) {
        this.players = new ArrayList<Player>(romaPlayerManager.players);
        this.turnManager = manager;
    }

    @Override
    public Collection<Player> getPlayers() {
        return Collections.unmodifiableList(players);
    }

    @Override
    public Player getPlayer(int playerNum) {
        return players.get(playerNum);
    }

    @Override
    public Player getCurrentPlayer() {
        return players.get(turnManager.getCurrentTurnNo() % Rules.NUM_PLAYERS);
    }

    @Override
    public Collection<Player> getOpponents(Player player) {
        Collection<Player> opponents = new LinkedList<Player>(players);
        opponents.remove(player);
        return opponents;
    }
}
