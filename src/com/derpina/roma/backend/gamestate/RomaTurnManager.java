/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.Rules;
import com.derpina.roma.core.TurnManager;

/**
 *
 * @author Matt
 */
public class RomaTurnManager implements TurnManager {

    int turnNumber;

    public RomaTurnManager() {
        turnNumber = Rules.STARTING_TURN_NUMBER;
    }

    RomaTurnManager(RomaTurnManager romaTurnManager) {
        this.turnNumber = romaTurnManager.turnNumber;
    }

    @Override
    public int getCurrentTurnNo() {
        return turnNumber;
    }

    @Override
    public void incrementTurn() {
        turnNumber++;
    }
}
