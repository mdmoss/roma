package com.derpina.roma.backend.gamestate;

import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.backend.modifiers.DefenceModifier;
import com.derpina.roma.backend.modifiers.DiscModifier;
import com.derpina.roma.backend.modifiers.TemporaryModifier;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.*;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Delegates all GameState methods to Roma managers.
 *
 * @author Matthew Moss <mdm@cse.unsw.edu.au>
 * @author Lasath Fernando <edu@lasath.org>
 */
public class AdvancedGameState implements LocalGameState, Cloneable {

    TurnManager turnManager;
    DiceManager actionDiceManager;
    DiceDiscManager diceDiscManager;
    PlayerManager playerManager;
    MoneyManager moneyManager;
    VictoryPointManager victoryPointManager;
    DiceAssociationManager diceAssociationManager;
    GameCardManager gameCardManager;
    GamePhaseManager gamePhaseManager;
    ObjectPropertiesManager objectPropertiesManager;
    TimeManager timeManager;

    public AdvancedGameState(ObjectBuilder builder) {

        this.turnManager = builder.createTurnManager();
        this.actionDiceManager = builder.createActionDiceManager();
        this.diceDiscManager = builder.createDiceDiscManager();

        this.playerManager = builder.createPlayerManager(turnManager);
        this.moneyManager = builder.createMoneyManager(playerManager);
        this.victoryPointManager = builder.createVictoryPointManager(playerManager);

        this.diceAssociationManager = builder.createDiceAssociationManager(diceDiscManager, actionDiceManager);
        this.gameCardManager = builder.createCardManager(playerManager, diceDiscManager, diceAssociationManager);
        this.gamePhaseManager = builder.createGamePhaseManager(playerManager, victoryPointManager);

        this.objectPropertiesManager = builder.createObjectPropertiesManager(gameCardManager);
        this.timeManager = builder.createTimeManager();
    }

    private AdvancedGameState(AdvancedGameState sourceState) {
        this.turnManager = new RomaTurnManager((RomaTurnManager) sourceState.turnManager);
        this.actionDiceManager = new RomaDiceManager((RomaDiceManager) sourceState.actionDiceManager);
        this.diceDiscManager = new RomaDiceDiscManager((RomaDiceDiscManager) sourceState.diceDiscManager);

        this.playerManager = new RomaPlayerManager((RomaPlayerManager) sourceState.playerManager, this.turnManager);
        this.moneyManager = new RomaMoneyManager((RomaMoneyManager) sourceState.moneyManager, this.playerManager);
        this.victoryPointManager = new RomaVictoryPointManager((RomaVictoryPointManager) sourceState.victoryPointManager, this.playerManager);

        this.diceAssociationManager = new RomaDiceAssociationManager((RomaDiceAssociationManager) sourceState.diceAssociationManager, this.diceDiscManager, this.actionDiceManager);
        this.gameCardManager = new RomaGameCardManager((RomaGameCardManager) sourceState.gameCardManager, this.playerManager, this.diceDiscManager, this.diceAssociationManager);
        this.gamePhaseManager = new RomaGamePhaseManager((RomaGamePhaseManager) sourceState.gamePhaseManager, this.playerManager, this.victoryPointManager);

        this.objectPropertiesManager = new RomaModificationManager((RomaModificationManager) sourceState.objectPropertiesManager, this.gameCardManager);
        this.timeManager = new RomaTimeManager((RomaTimeManager) sourceState.timeManager);
    }

    @Override
    public int getCurrentTurnNo() {
        return turnManager.getCurrentTurnNo();
    }

    @Override
    public void incrementTurn() {
        turnManager.incrementTurn();
    }

    @Override
    public Collection<Dice> getActionDice() {
        return actionDiceManager.getActionDice();
    }

    @Override
    public void useUpDice(Dice value) {
        actionDiceManager.useUpDice(value);
    }

    @Override
    public int getValueOf(Dice dice) {
        return actionDiceManager.getValueOf(dice);
    }

    @Override
    public List<DiceDisc> getDiceDiscs() {
        return diceDiscManager.getDiceDiscs();
    }

    @Override
    public Collection<Player> getPlayers() {
        return playerManager.getPlayers();
    }

    @Override
    public Player getPlayer(int playerNum) {
        return playerManager.getPlayer(playerNum);
    }

    @Override
    public Player getCurrentPlayer() {
        return playerManager.getCurrentPlayer();
    }

    @Override
    public Collection<Player> getOpponents(Player player) {
        return playerManager.getOpponents(player);
    }

    @Override
    public void earnPlayerMoney(Player player, int amount) {
        moneyManager.earnPlayerMoney(player, amount);
    }

    @Override
    public void spendPlayerMoney(Player player, int amount) {
        moneyManager.spendPlayerMoney(player, amount);
    }

    @Override
    public int getPlayerMoney(Player player) {
        return moneyManager.getPlayerMoney(player);
    }

    @Override
    public void setPlayerPoints(Player p, int value) {
        victoryPointManager.setPlayerPoints(p, value);
    }

    @Override
    public void setPool(int numPoints) {
        victoryPointManager.setPool(numPoints);
    }

    @Override
    public int getPlayerPoints(Player p) {
        return victoryPointManager.getPlayerPoints(p);
    }

    @Override
    public int getPool() {
        return victoryPointManager.getPool();
    }

    @Override
    public void moveVictoryPoints(Player from, Player to, int amount) {
        victoryPointManager.moveVictoryPoints(from, to, amount);
    }

    @Override
    public void movePointsToPool(Player from, int amount) {
        victoryPointManager.movePointsToPool(from, amount);
    }

    @Override
    public void movePointsFromPool(Player to, int amount) {
        victoryPointManager.movePointsFromPool(to, amount);
    }

    @Override
    public Collection<CardInstance> getCards(Location location, CardType type) {
        return gameCardManager.getCards(location, type);
    }

    @Override
    public Collection<CardInstance> getOpposingCards(Location location, CardType type) {
        return gameCardManager.getOpposingCards(location, type);
    }

    @Override
    public void makeCardFloating(CardInstance card) {
        gameCardManager.makeCardFloating(card);
    }

    @Override
    public void moveCardToDisc(CardInstance card, DiceDisc destination) {
        gameCardManager.moveCardToDisc(card, destination);
    }

    @Override
    public void moveCardToDiscardPile(CardInstance card) {
        gameCardManager.moveCardToDiscardPile(card);
    }

    @Override
    public void moveCardToDrawPile(CardInstance card) {
        gameCardManager.moveCardToDrawPile(card);
    }

    @Override
    public void moveCardToHand(CardInstance card, Player hand) {
        gameCardManager.moveCardToHand(card, hand);
    }

    @Override
    public Player getOwner(CardInstance card) {
        return gameCardManager.getOwner(card);
    }

    @Override
    public Collection<CardInstance> getOppositeLaidCards(CardInstance card, CardType type) {
        return gameCardManager.getOppositeLaidCards(card, type);
    }

    @Override
    public Collection<CardInstance> getCardsOnDisc(DiceDisc disc, Player owner) {
        return gameCardManager.getCardsOnDisc(disc, owner);
    }

   @Override
    public DiceDisc getCardLocation(CardInstance card) {
        return gameCardManager.getCardLocation(card);
    }

    @Override
    public Dice getActionDiceOfValue(DiceDisc disc) {
        return diceAssociationManager.getActionDiceOfValue(disc);
    }

    @Override
    public int getDiscIntegerValue(DiceDisc disc) {
        return diceAssociationManager.getDiscIntegerValue(disc);
    }

    @Override
    public DiceDisc getDiceDiscOfValue(Dice dice) {
        return diceAssociationManager.getDiceDiscOfValue(dice);
    }

    @Override
    public boolean isComplete() {
        return gamePhaseManager.isComplete();
    }

    @Override
    public Player getWinner() {
        return gamePhaseManager.getWinner();
    }

    @Override
    public List<CostModifier> getCostModifiers() {
        return objectPropertiesManager.getCostModifiers();
    }

    @Override
    public List<DefenceModifier> getDefenceModifiers() {
        return objectPropertiesManager.getDefenceModifiers();
    }

    @Override
    public List<DiscModifier> getDiscModifiers() {
        return objectPropertiesManager.getDiscModifiers();
    }

    @Override
    public Collection<TemporaryModifier> getAllModifiers() {
        return objectPropertiesManager.getAllModifiers();
    }

    @Override
    public void setCostModifiers(List<CostModifier> modifiers) {
        objectPropertiesManager.setCostModifiers(modifiers);
    }

    @Override
    public void setDefenceModifiers(List<DefenceModifier> modifiers) {
        objectPropertiesManager.setDefenceModifiers(modifiers);
    }

    @Override
    public void setDiscModifiers(List<DiscModifier> modifiers) {
        objectPropertiesManager.setDiscModifiers(modifiers);
    }

    @Override
    public Collection<CardInstance> getAdjacentCards(CardInstance card) {
        return gameCardManager.getAdjacentCards(card);
    }

    @Override
    public DiceDisc getDiceDiscOfValue(int value) {
        return diceAssociationManager.getDiceDiscOfValue(value);
    }

    @Override
    public boolean isBribeDisc(DiceDisc disc) {
        return diceAssociationManager.isBribeDisc(disc);
    }

    @Override
    public DiceDisc getBribeDisc() {
        return diceAssociationManager.getBribeDisc();
    }

    @Override
    public Dice getBattleDie() {
        return this.actionDiceManager.getBattleDie();
    }

    @Override
    public void moveCardToTimeWarp(CardInstance card) {
        gameCardManager.moveCardToTimeWarp(card);
    }

    @Override
    public void schedule(Player player, CardInstance card, DiceDisc dest, int turn) {
        timeManager.schedule(player, card, dest, turn);
    }

    @Override
    public CardInstance getPendingCardPlacements(LocalGameState gameState, Player player, DiceDisc loc, int turn) {
        return timeManager.getPendingCardPlacements(gameState, player, loc, turn);
    }

    @Override
    public void moveCardToDisc(CardInstance card, DiceDisc destination, Player player) {
        gameCardManager.moveCardToDisc(card, destination, player);
    }

    @Override
    public Collection<CardInstance> getCards(Location location, CardType type, Player owner) {
        return gameCardManager.getCards(location, type, owner);
    }

    @Override
    public LocalGameState clone() {
        AdvancedGameState newState = new AdvancedGameState(this);
        return newState;
    }

    @Override
    public void setValueOfDice(Dice dice, int value) {
        actionDiceManager.setValueOfDice(dice, value);
    }

    @Override
    public void setNextBattleDie(int roll) {
        actionDiceManager.setNextBattleDie(roll);
    }

    @Override
    public void maskDice(Dice dice) {
        actionDiceManager.maskDice(dice);
    }

    @Override
    public void revealDice(Dice dice) {
        actionDiceManager.revealDice(dice);
    }

    @Override
    public Dice getDiceOfValue(int roll) {
        return actionDiceManager.getDiceOfValue(roll);
    }

    @Override
    public void useUpRemainingDice() {
        actionDiceManager.useUpRemainingDice();
    }

    @Override
    public void unmaskAllDice() {
        actionDiceManager.unmaskAllDice();
    }

    @Override
    public void setActionDiceValues(Collection<Integer> values) {
        actionDiceManager.setActionDiceValues(values);
    }

    @Override
    public void addCardToGame(CardInstance card) {
        gameCardManager.addCardToGame(card);
    }

    @Override
    public int getCostFor(CardInstance card, LocalGameState gameState) {
        return objectPropertiesManager.getCostFor(card, gameState);
    }

    @Override
    public int getDefenceFor(CardInstance card, LocalGameState gameState) {
        return objectPropertiesManager.getDefenceFor(card, gameState);
    }

    @Override
    public boolean getIsBlocked(DiceDisc disc, Player asking, LocalGameState gameState) {
        return objectPropertiesManager.getIsBlocked(disc, asking, gameState);
    }
}
