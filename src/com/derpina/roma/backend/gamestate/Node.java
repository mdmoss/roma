/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardLocation;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Token;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
class Node implements CardLocation {

    private List<Node> children;
    private Collection<CardInstance> cards;
    private final Token identifier;

    Node(Token identifier) {
        this.children = new ArrayList<Node>();
        this.identifier = identifier;
        this.cards = new ArrayList<CardInstance>();
    }

    @Override
    public void insertCard(CardInstance card) {
        this.cards.add(card);
    }

    @Override
    public void removeCard(CardInstance card) {
        assert (cards.contains(card));
        cards.remove(card);
    }

    @Override
    public Collection<CardInstance> getAllCards(CardType ofType) {
        Collection<CardInstance> result = new ArrayList<CardInstance>();

        for (CardInstance card : this.cards) {
            if (ofType == CardType.ANY || card.getType() == ofType) {
                result.add(card);
            }
        }

        for (CardLocation location : children) {
            result.addAll(location.getAllCards(ofType));
        }

        return result;
    }

    @Override
    public Token getIdentifier() {
        return this.identifier;
    }

    @Override
    public void addChild(CardLocation child) {
        this.children.add((Node)child);
    }

    public void removeChild (Token child) {
        ArrayList<Node> toDelete = new ArrayList<Node>();
        for (Node n : this.children) {
            if (n.identifier == child) {
                toDelete.add(n);
            }
        }
        this.children.removeAll(toDelete);
    }

    @Override
    public CardLocation getChild(Token identifier) {
        for (CardLocation child : children) {
            if (child.getIdentifier().equals(identifier)) {
                return child;
            }
        }
        throw new IllegalArgumentException("Requested token is not immidiate child");
    }

    @Override
    public Node clone () {

        Node newThis = new Node(identifier);
        newThis.cards = new ArrayList<CardInstance>();
        for (CardInstance card : this.cards) {
            newThis.cards.add(card.clone());
        }

        for (Node loc : children) {
            newThis.children.add((loc).clone());
        }

        return newThis;
    }

    public void rebuildCache (HashMap<CardInstance, CardLocation> cache) {

        for (Node child : children) {
            child.rebuildCache(cache);
        }

        for (CardInstance card : cards) {
            cache.put(card, this);
        }
    }

    @Override
    public String toString() {
        return identifier.toString();
    }
}
