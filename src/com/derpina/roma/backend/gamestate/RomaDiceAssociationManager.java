/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.core.*;
import java.util.*;

/**
 *
 * @author Matt
 */
public class RomaDiceAssociationManager implements DiceAssociationManager {

    Map<Integer, DiceDisc> associatedValues;
    DiceManager actionDiceManager;
    DiceDiscManager diceDiscManager;

    private DiceDisc bribeDisc = null;

    public RomaDiceAssociationManager(ObjectBuilder builder, DiceDiscManager diceDiscManager, DiceManager actionDiceManager) {
        associatedValues = new HashMap<Integer, DiceDisc>(Rules.NUM_CARD_FIELDS);
        this.actionDiceManager = actionDiceManager;
        this.diceDiscManager = diceDiscManager;

        Iterator discIterator = diceDiscManager.getDiceDiscs().iterator();
        for (int i = 1; i <= Rules.NUM_DICE_SIDES && discIterator.hasNext(); i++) {
            associatedValues.put(i, (DiceDisc) discIterator.next());
        }
        if (discIterator.hasNext()) {
            bribeDisc = (DiceDisc) discIterator.next();
        }
    }

    RomaDiceAssociationManager(RomaDiceAssociationManager romaDiceAssociationManager, DiceDiscManager diceDiscManager, DiceManager actionDiceManager) {
        this.associatedValues = new HashMap<Integer, DiceDisc> (romaDiceAssociationManager.associatedValues);
        this.actionDiceManager = actionDiceManager;
        this.diceDiscManager = diceDiscManager;
        this.bribeDisc = romaDiceAssociationManager.bribeDisc;
    }

    @Override
    public int getDiscIntegerValue(DiceDisc disc) {
        for (Integer i : associatedValues.keySet()) {
            if (associatedValues.get(i) == disc) {
                return i;
            }
        }
        if (isBribeDisc(disc)) {
            return Constants.BRIBE_DISC_INT;
        }
        return -1;
    }

    @Override
    public Dice getActionDiceOfValue(DiceDisc disc) {
        for (Dice d : actionDiceManager.getActionDice()) {
            if (actionDiceManager.getValueOf(d) == getDiscIntegerValue(disc)) {
                return d;
            }
        }
        return null;
    }

    @Override
    public DiceDisc getDiceDiscOfValue(Dice dice) {
        return (associatedValues.get(actionDiceManager.getValueOf(dice)));
    }

    @Override
    public DiceDisc getDiceDiscOfValue(int value) {
        DiceDisc result = associatedValues.get(value);
        if (result != null) {
            return result;
        } else if (value == Constants.BRIBE_DISC_INT) {
            return bribeDisc;
        }
        return null;
    }

    @Override
    public boolean isBribeDisc(DiceDisc disc) {
        return (disc == bribeDisc);
    }

    @Override
    public DiceDisc getBribeDisc() {
        return bribeDisc;
    }
}
