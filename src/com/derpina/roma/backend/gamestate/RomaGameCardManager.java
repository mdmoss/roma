/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.gamestate;

import com.derpina.roma.backend.HumanPlayer;
import com.derpina.roma.core.ObjectBuilder;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class RomaGameCardManager implements GameCardManager {

    private DiceDiscManager diceDiscManager;
    private PlayerManager playerManager;
    private DiceAssociationManager diceAssociationManager;
    Node root;
    HashMap<CardInstance, CardLocation> locationCache;


    public RomaGameCardManager(ObjectBuilder builder, PlayerManager turnManager, DiceDiscManager diceDiscManager, DiceAssociationManager diceAssociationManager) {
        this.playerManager = turnManager;
        this.diceDiscManager = diceDiscManager;
        this.diceAssociationManager = diceAssociationManager;
        this.locationCache = new HashMap<CardInstance, CardLocation>();
        this.buildTree(builder, turnManager, diceDiscManager);
    }

    RomaGameCardManager(RomaGameCardManager romaGameCardManager, PlayerManager playerManager, DiceDiscManager diceDiscManager, DiceAssociationManager diceAssociationManager) {
        this.diceDiscManager = diceDiscManager;
        this.playerManager = playerManager;
        this.diceAssociationManager = diceAssociationManager;
        this.root = romaGameCardManager.root.clone();

        // Commence Awesomeness
        CardLocation discardPile = root.getChild(playerManager.getPlayer(0)).getChild(Location.DISCARD_PILE);
        CardLocation floatingCards = root.getChild(playerManager.getPlayer(0)).getChild(Location.FLOATING);
        CardLocation drawPile = root.getChild(playerManager.getPlayer(0)).getChild(Location.DRAW_PILE);
        CardLocation timeWarp = root.getChild(playerManager.getPlayer(0)).getChild(Location.TIME_WARP);

        for (Player p : playerManager.getPlayers()) {

            ((Node)root.getChild(p)).removeChild(Location.DRAW_PILE);
            ((Node)root.getChild(p)).removeChild(Location.DISCARD_PILE);
            ((Node)root.getChild(p)).removeChild(Location.FLOATING);
            ((Node)root.getChild(p)).removeChild(Location.TIME_WARP);

            ((Node)root.getChild(p)).addChild(drawPile);
            ((Node)root.getChild(p)).addChild(floatingCards);
            ((Node)root.getChild(p)).addChild(discardPile);
            ((Node)root.getChild(p)).addChild(timeWarp);
        }

        this.locationCache = new HashMap<CardInstance, CardLocation>();
        this.root.rebuildCache(locationCache);
    }

    @Override
    public Collection<CardInstance> getCards(Location location, CardType type, Player owner) {
        return root.getChild(owner).getChild(location).getAllCards(type);
    }

    @Override
    public Collection<CardInstance> getCards(Location location, CardType required) {
        Player player = this.playerManager.getCurrentPlayer();
        return getCards(location, required, player);

    }

    @Override
    public Collection<CardInstance> getOpposingCards(Location location, CardType type) {
        Collection<CardInstance> result = new ArrayList<CardInstance>();

        for (Player p : this.playerManager.getPlayers()) {
            if (p != this.playerManager.getCurrentPlayer()) {

                result.addAll(this.root.getChild(p).
                        getChild(location).getAllCards(type));
            }
        }
        return result;
    }

    @Override
    public void makeCardFloating(CardInstance card) {
        removeCard(card);
        insertCard (card, this.root.getChild(playerManager.getCurrentPlayer()).
                getChild(Location.FLOATING));
    }

    @Override
    public void moveCardToDisc(CardInstance card, DiceDisc destination) {
        moveCardToDisc(card, destination, playerManager.getCurrentPlayer());
    }

        @Override
    public void moveCardToDisc(CardInstance card, DiceDisc destination, Player player) {
        removeCard(card);
        insertCard (card, root.getChild(player).
                                 getChild(Location.DICE_DISC).
                                 getChild(destination));
    }

    @Override
    public void moveCardToDiscardPile(CardInstance card) {
        removeCard(card);
        insertCard (card, root.getChild(playerManager.getCurrentPlayer()).
                                      getChild(Location.DISCARD_PILE));
    }

    @Override
    public void moveCardToDrawPile(CardInstance card) {
        removeCard(card);
        insertCard (card, root.getChild(playerManager.getCurrentPlayer()).
                                          getChild(Location.DRAW_PILE));
    }

    @Override
    public void moveCardToHand(CardInstance card, Player hand) {
        removeCard(card);
        insertCard (card, root.getChild(hand).getChild(Location.PLAYER_HAND));
    }

    @Override
    public void moveCardToTimeWarp(CardInstance card) {
        removeCard(card);
        insertCard (card, root.getChild(playerManager.getCurrentPlayer()).getChild(Location.TIME_WARP));
    }

    public void insertCard (CardInstance card, CardLocation location) {
        location.insertCard(card);
        locationCache.put(card, location);
    }

    public void removeCard(CardInstance card) {
        assert (locationCache.containsKey(card));
        locationCache.get(card).removeCard(card);// Remove from its current node
        locationCache.remove(card);// Remove the Cache reference
    }

    @Override
    public Player getOwner(CardInstance card) {
        Location[] nonShared = new Location[] {
            Location.DICE_DISC,
            Location.PLAYER_HAND
        };

        for (Location location : nonShared) {
            for (Player player : playerManager.getPlayers()) {
                if (root.getChild(player).getChild(location).
                        getAllCards(CardType.ANY).contains(card)) {

                    return player;
                }
            }
        }

        return null;
    }

    private void buildTree (ObjectBuilder builder, PlayerManager playerManager, DiceDiscManager diceDiscManager) {
        this.root = new Node(null);
        CardLocation discardPile = new Node(Location.DISCARD_PILE);
        CardLocation floatingCards = new Node(Location.FLOATING);
        CardLocation drawPile = new Node(Location.DRAW_PILE);
        CardLocation timeWarp = new Node(Location.TIME_WARP);
        for (CardInstance card : builder.getInitialDrawPile()) {
            insertCard(card, drawPile);
        }

        for (Player p : playerManager.getPlayers()) {
            CardLocation playerRoot = new Node(p);
            root.addChild(playerRoot);

            playerRoot.addChild(drawPile);
            playerRoot.addChild(discardPile);
            playerRoot.addChild(floatingCards);
            playerRoot.addChild(timeWarp);

            CardLocation hand = new Node(Location.PLAYER_HAND);
            playerRoot.addChild(hand);

            CardLocation discs = new Node(Location.DICE_DISC);
            playerRoot.addChild(discs);
            for (DiceDisc d : diceDiscManager.getDiceDiscs()) {
                CardLocation discN = new Node(d);
                discs.addChild(discN);
            }
        }
    }

    @Override
    public Collection<CardInstance> getOppositeLaidCards(CardInstance card, CardType type) {
        CardLocation search = locationCache.get(card);
        Collection<CardInstance> oppositeCards = new LinkedList<CardInstance>();
        for (Player p : playerManager.getPlayers()) {
            if (p != getOwner(card)) {
                // It's a valid search space
                CardLocation opp = root.getChild(p).
                        getChild(Location.DICE_DISC).
                        getChild(search.getIdentifier());
                oppositeCards.addAll(opp.getAllCards(type));
            }
        }
        return oppositeCards;
    }

    @Override
    public Collection<CardInstance> getCardsOnDisc(DiceDisc disc, Player owner) {
        CardLocation loc = root.getChild(owner).
                getChild(Location.DICE_DISC).
                getChild(disc);
        Collection<CardInstance> cards = loc.getAllCards(CardType.ANY);
        return cards;
    }

    @Override
    public DiceDisc getCardLocation(CardInstance card) {
        for (Player p : playerManager.getPlayers()) {
            for (DiceDisc d : diceDiscManager.getDiceDiscs()) {
                if (root.getChild(p).getChild(Location.DICE_DISC).getChild(d).getAllCards(CardType.ANY).contains(card)) {
                    return d;
                }
            }
        }
        return null;
    }

    @Override
    public Collection<CardInstance> getAdjacentCards(CardInstance card) {
        Collection<CardInstance> result = new ArrayList<CardInstance>();
        DiceDisc location = getCardLocation(card);

        if (location != null) {
            int value = diceAssociationManager.getDiscIntegerValue(location);

            DiceDisc check = diceAssociationManager.getDiceDiscOfValue(value + 1);
            if (check != null) {
                result.addAll(getCardsOnDisc(check, getOwner(card)));
            }

            check = diceAssociationManager.getDiceDiscOfValue(value - 1);
            if (check != null) {
                result.addAll(getCardsOnDisc(check, getOwner(card)));
            }
        }

        return result;
    }

    @Override
    public void addCardToGame(CardInstance card) {
        try {
            card.setToken(card.getGameCard().getClass().newInstance());
        } catch (InstantiationException ex) {
            Logger.getLogger(RomaGameCardManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(RomaGameCardManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.insertCard(card, root.getChild(playerManager.getCurrentPlayer()).getChild(Location.TIME_WARP));
    }
}
