/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.gamestate;

import com.derpina.roma.backend.cards.*;
import com.derpina.roma.core.*;
import framework.cards.Card;
import framework.interfaces.GameState;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matt
 */
public class GameStateAdaptor implements GameState {

    AdvancedGameState ourState;

    static EnumMap<Card, Class<? extends GameCard>> cardClasses = new EnumMap<Card, Class<? extends GameCard>>(Card.class);
    static HashMap<GameCard, Card> cardEnums = new HashMap<GameCard, Card>();

    Player[] players;
    DiceDisc[] diceDiscs;

    public GameStateAdaptor(LocalGameState ourState) {
        this.ourState = (AdvancedGameState) ourState;
        this.players = new Player[ourState.getPlayers().size()];
        players = ourState.getPlayers().toArray(players);
        this.diceDiscs = new DiceDisc[ourState.getDiceDiscs().size()];
        diceDiscs = ourState.getDiceDiscs().toArray(diceDiscs);
    }

    @Override
    public int getWhoseTurn() {
        return ourState.getCurrentTurnNo() % Rules.NUM_PLAYERS;
    }

    @Override
    public void setWhoseTurn(int player) {
        while (getWhoseTurn() != player) {
            ourState.incrementTurn();
        }
    }

    @Override
    public List<Card> getDeck() {

        List<Card> result = new LinkedList<Card>();

        Collection<CardInstance> deck = ourState.getCards(Location.DRAW_PILE, CardType.ANY);

        for (CardInstance card : deck) {
            result.add(getEnum(card));
        }

        return result;
    }

    @Override
    public void setDeck(List<Card> deck) {
        CardLocation root = getRootNode();
        CardLocation deckNode = root.getChild(players[0]).getChild(Location.DRAW_PILE);
        setNodeCards(deckNode, deck);
    }

    @Override
    public List<Card> getDiscard() {
        List<Card> result = new LinkedList<Card>();
        Collection<CardInstance> deck = ourState.getCards(Location.DISCARD_PILE, CardType.ANY);
        for (CardInstance card : deck) {
            result.add(getEnum(card));
        }
        return result;
    }

    @Override
    public void setDiscard(List<Card> discard) {
        CardLocation root = getRootNode();
        CardLocation deckNode = root.getChild(players[0]).getChild(Location.DISCARD_PILE);
        setNodeCards(deckNode, discard);
    }

    @Override
    public int getPlayerSestertii(int playerNum) {
        return ourState.getPlayerMoney(players[playerNum]);
    }

    @Override
    public void setPlayerSestertii(int playerNum, int amount) {

        int current = ourState.getPlayerMoney(players[playerNum]);
        int change = current - amount;
        ourState.spendPlayerMoney(players[playerNum], change);
    }

    @Override
    public int getPlayerVictoryPoints(int playerNum) {
        return ourState.getPlayerPoints(players[playerNum]);
    }

    @Override
    public void setPlayerVictoryPoints(int playerNum, int points) {

        ourState.movePointsToPool(players[playerNum], ourState.getPlayerPoints(players[playerNum]));
        ourState.movePointsFromPool(players[playerNum], points);
    }

    @Override
    public Collection<Card> getPlayerHand(int playerNum) {

        Collection<Card> hand = new LinkedList<Card>();

        for (CardInstance handCard : getRootNode().getChild(players[playerNum]).getChild(Location.PLAYER_HAND).getAllCards(CardType.ANY)) {
            hand.add(getEnum(handCard));
        }

        return hand;
    }


    {
        cardClasses.put(Card.AESCULAPINUM, Aesculapinum.class);
        cardClasses.put(Card.ARCHITECTUS, Architectus.class);
        cardClasses.put(Card.BASILICA, Basilica.class);
        cardClasses.put(Card.CENTURIO, Centurio.class);
        cardClasses.put(Card.CONSILIARIUS, Consiliarius.class);
        cardClasses.put(Card.CONSUL, Consul.class);
        cardClasses.put(Card.ESSEDUM, Essedum.class);
        cardClasses.put(Card.FORUM, Forum.class);
        cardClasses.put(Card.GLADIATOR, Gladiator.class);
        cardClasses.put(Card.HARUSPEX, Haruspex.class);
        cardClasses.put(Card.LEGAT, Legat.class);
        cardClasses.put(Card.LEGIONARIUS, Legionarius.class);
        cardClasses.put(Card.MACHINA, Machina.class);
        cardClasses.put(Card.MERCATOR, Mercator.class);
        cardClasses.put(Card.MERCATUS, Mercatus.class);
        cardClasses.put(Card.NERO, Nero.class);
        cardClasses.put(Card.ONAGER, Onager.class);
        cardClasses.put(Card.PRAETORIANUS, Praetorianus.class);
        cardClasses.put(Card.SENATOR, Senetor.class);
        cardClasses.put(Card.SICARIUS, Sicarius.class);
        cardClasses.put(Card.SCAENICUS, Scaenicus.class);
        cardClasses.put(Card.TEMPLUM, Templum.class);
        cardClasses.put(Card.TRIBUNUSPLEBIS, TribunusPlebis.class);
        cardClasses.put(Card.TURRIS, Turris.class);
        cardClasses.put(Card.VELITES, Velites.class);
        cardClasses.put(Card.KAT, Kat.class);
        cardClasses.put(Card.GRIMREAPER, GrimReaper.class);
        cardClasses.put(Card.TELEPHONEBOX, TelephoneBox.class);
    }

    @Override
    public void setPlayerHand(int playerNum, Collection<Card> hand) {
        CardLocation handNode = getRootNode().getChild(this.players[playerNum]).getChild(Location.PLAYER_HAND);
        setNodeCards(handNode, hand);
    }

    private void setNodeCards(CardLocation node, Collection<Card> cards) {

        RomaGameCardManager gameCardManager = (RomaGameCardManager) ourState.gameCardManager;

        Collection<CardInstance> currentHand = node.getAllCards(CardType.ANY);
        for (CardInstance gameCard : currentHand) {
            gameCardManager.removeCard(gameCard);
        }
        for (Card c : cards) {

            if (c != Card.NOT_A_CARD) {

                try {
                   gameCardManager.insertCard (cardClasses.get(c).newInstance().createInstance(), node);
                } catch (InstantiationException ex) {
                    Logger.getLogger(GameStateAdaptor.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(GameStateAdaptor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    @Override
    public Card[] getPlayerCardsOnDiscs(int playerNum) {
        Card[] cards = new Card[diceDiscs.length];
        int i = 0;
        for (DiceDisc disc : diceDiscs) {
            if (ourState.getCardsOnDisc(disc, players[playerNum]).isEmpty()) {
                cards[i] = Card.NOT_A_CARD;
            } else {
                cards[i] = getEnum(ourState.getCardsOnDisc(disc, players[playerNum]).iterator().next());
            }
            i++;
        }
        return cards;
    }

    @Override
    public void setPlayerCardsOnDiscs(int playerNum, Card[] discCards) {

        CardLocation discsNode = getRootNode().getChild(players[playerNum]).getChild(Location.DICE_DISC);

        for (int i = 0; i < Rules.NUM_CARD_FIELDS; i++) {
            Collection<Card> card = new ArrayList<Card>(1);
            card.add(discCards[i]);
            setNodeCards(discsNode.getChild(ourState.getDiceDiscs().get(i)), card);
        }
    }

    @Override
    public int[] getActionDice() {
        int[] dice = new int[ourState.getActionDice().size()];
        int i = 0;
        for (Dice actionDice : ourState.getActionDice()) {
            dice[i] = ourState.getValueOf(actionDice);
            i++;
        }

        return dice;
    }

    @Override
    public void setActionDice(int[] dice) {
        Collection derp = new ArrayList();
        for (int i : dice) {
            derp.add(i);
        }
        ourState.setActionDiceValues(derp);
    }

    @Override
    public int getPoolVictoryPoints() {
        return ourState.getPool();
    }

    @Override
    public boolean isGameCompleted() {
        return ourState.isComplete();
    }

    {
        cardEnums.put(new Aesculapinum(), Card.AESCULAPINUM);
        cardEnums.put(new Architectus(), Card.ARCHITECTUS);
        cardEnums.put(new Basilica(), Card.BASILICA);
        cardEnums.put(new Centurio(), Card.CENTURIO);
        cardEnums.put(new Consiliarius(), Card.CONSILIARIUS);
        cardEnums.put(new Consul(), Card.CONSUL);
        cardEnums.put(new Essedum(), Card.ESSEDUM);
        cardEnums.put(new Forum(), Card.FORUM);
        cardEnums.put(new Gladiator(), Card.GLADIATOR);
        cardEnums.put(new Haruspex(), Card.HARUSPEX);
        cardEnums.put(new Legat(), Card.LEGAT);
        cardEnums.put(new Legionarius(), Card.LEGIONARIUS);
        cardEnums.put(new Machina(), Card.MACHINA);
        cardEnums.put(new Mercator(), Card.MERCATOR);
        cardEnums.put(new Mercatus(), Card.MERCATUS);
        cardEnums.put(new Nero(), Card.NERO);
        cardEnums.put(new Onager(), Card.ONAGER);
        cardEnums.put(new Praetorianus(), Card.PRAETORIANUS);
        cardEnums.put(new Scaenicus(), Card.SCAENICUS);
        cardEnums.put(new Senetor(), Card.SENATOR);
        cardEnums.put(new Sicarius(), Card.SICARIUS);
        cardEnums.put(new Templum(), Card.TEMPLUM);
        cardEnums.put(new TribunusPlebis(), Card.TRIBUNUSPLEBIS);
        cardEnums.put(new Turris(), Card.TURRIS);
        cardEnums.put(new Velites(), Card.VELITES);
        cardEnums.put(new GrimReaper(), Card.GRIMREAPER);
        cardEnums.put(new Kat(), Card.KAT);
        cardEnums.put(new TelephoneBox(), Card.TELEPHONEBOX);
    }

    public static Card getEnum (CardInstance card) {

        for (GameCard ours : cardEnums.keySet()) {
            if (ours.getClass() == card.getGameCard().getClass()) {
                return cardEnums.get(ours);
            }
        }
        assert (false);
        return null;
    }

    public static GameCard getGameCard (Card card) {
        try {
            return cardClasses.get(card).newInstance();
        } catch (InstantiationException ex) {
            Logger.getLogger(GameStateAdaptor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(GameStateAdaptor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private CardLocation getRootNode() {
        return ((RomaGameCardManager) ourState.gameCardManager).root;
    }

    public LocalGameState getGameState() {
        return this.ourState;
    }

    public void setGameState(LocalGameState ourState) {
        this.ourState = (AdvancedGameState) ourState;
    }
}
