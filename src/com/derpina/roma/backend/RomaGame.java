/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionStartChain;
import com.derpina.roma.backend.actions.ActionStartGame;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.utils.InstantCollection;
import java.util.Stack;

/**
 *
 * @author Lasath Fernando
 * @author Matthew Moss
 */
public class RomaGame implements com.derpina.roma.core.GameEngine {

    LocalGameState state;

    // TODO change this stupid name dammit
//    Action currentlyProcessingAction;

//    Deque<Action> actionQueue;
    private boolean hasStarted = false;

    public RomaGame() {
        state = new AdvancedGameState(new RomaBuilder());
//        actionQueue = new LinkedList<Action>();
//        currentlyProcessingAction = new ActionStartChain(state);
        actions.push((List)InstantCollection.get(new ActionStartGame(state)));
        executeActions();
        actions.push((List)InstantCollection.get(new ActionStartChain(state)));
    }

    @Override
    public Collection<Choice> getValidIntents() {

        Collection<Choice> result = new ArrayList<Choice>();
        if (!gameState().isComplete()) {
            result.addAll(actions.peek().get(0).getIntents());
        }
        for (@SuppressWarnings("unused")
        Choice choice : result) {
            // System.out.println(choice.getType());
        }
        return result;
    }

    // TODO: Make both these methods catch all exceptions, print an
    // apologetic message to the user and continue with the next top
    // level actions.
    @Override
    public void handleChoice(Choice chosen) {

        AbstractChoiceFactory owner = chosen.getOwner();
        owner.chosen(chosen);

//        if(!this.hasStarted) {
////            actionQueue.addFirst(currentlyProcessingAction);
//            actions.push((List)InstantCollection.get(new ActionStartChain(state)));
//        }

        while (actions.peek().get(0).getIntents().isEmpty()) {
            executeActions();
            if (actions.isEmpty()) {
                actions.push((List)InstantCollection.get(new ActionStartChain(state)));
            }
        }

//        while (currentlyProcessingAction.getIntents().isEmpty()) {
//            assert (currentlyProcessingAction.isValid());
//            currentlyProcessingAction.execute();
//
//            // This reversal is needed to ensure actions are activated
//            // in the correct order. Actions are returned in activation
//            // order by next(), and so need to be reversed before being
//            // placed in the queue
//            List<Action> childBuffer = new LinkedList<Action>(
//                    currentlyProcessingAction.next());
//            Collections.reverse(childBuffer);
//            for (Action action : childBuffer) {
//                actionQueue.add(action);
//            }
//
//            if (!actionQueue.isEmpty()) {
//                currentlyProcessingAction = actionQueue.pop();
//            } else {
//                currentlyProcessingAction = new ActionStartChain(state);
//            }
//        }
    }

    Stack<List<Action>> actions = new Stack<List<Action>>();

    private void executeActions() {
        while (!actions.isEmpty()) {
            List<Action> frame = actions.peek();
            while (!frame.isEmpty()) {
                Action action = frame.get(0);
                if (!action.getIntents().isEmpty()) {
                    return;
                }

                assert (action.isValid());
                action.execute();

                if (!action.next().isEmpty()) {
                    actions.push((List) action.next());
                }

                frame.remove(0);
            }
            actions.remove(frame);
        }
    }

    @Override
    public LocalGameState gameState() {
        return state;
    }
}
