/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend;

import com.derpina.roma.backend.actions.ActionActivateCardsDisc;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionActivateMoneyDisc;
import com.derpina.roma.backend.actions.ActionEndTurn;
import com.derpina.roma.backend.actions.ActionActivateCard;
import com.derpina.roma.backend.actions.ActionPlaceCard;
import com.derpina.roma.backend.actions.ActionActivateBribeDisc;
import com.derpina.roma.backend.Mover.LinearChoiceInterface;
import com.derpina.roma.backend.gamestate.GameStateAdaptor;
import com.derpina.roma.core.*;
import com.derpina.roma.ui.console.Printer;
import framework.cards.Card;
import framework.interfaces.GameState;
import framework.interfaces.MoveMaker;
import framework.interfaces.activators.*;
import java.util.*;


/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class Mover extends TimeEngine implements MoveMaker {

    GameStateAdaptor theirState;
    LinearChoiceInterface choiceHandler;

    Mover(LinearChoiceInterface ui, GameState initial) {
        super(ui, null);
        theirState = (GameStateAdaptor)initial;

        choiceHandler = ui;
    }

    public static Mover getInstance(GameState gameState) {
        return new Mover(new LinearChoiceInterface(), gameState);
    }

    @Override
    public CardActivator chooseCardToActivate(int disc) throws UnsupportedOperationException {
        Action newAction = new ActionActivateCard(getActiveState());
        choiceHandler.purge();
        Collection<CardInstance> cards = getActiveState().getCardsOnDisc(getActiveState().getDiceDiscOfValue(disc), getActiveState().getCurrentPlayer());
        choiceHandler.put(ChoiceType.CHOOSE_CARD, cards.iterator().next());
        getActiveState().maskDice(getActiveState().getActionDiceOfValue(getActiveState().getDiceDiscOfValue(disc)));
        return new Activator(choiceHandler, getActiveState(), theirState.getPlayerCardsOnDiscs(theirState.getWhoseTurn())[disc - 1], newAction, this);
    }

    @Override
    public void activateCardsDisc(int diceToUse, Card chosen) throws UnsupportedOperationException {
        Dice toUse = getActiveState().getDiceOfValue(diceToUse);
        choiceHandler.put(ChoiceType.CHOOSE_ACTION_DICE, toUse);
        getActiveState().maskDice(toUse);
        CardInstance found = null;
        for (CardInstance card : getActiveState().getCards(Location.DRAW_PILE, CardType.ANY)) {
            if (GameStateAdaptor.getEnum(card) == chosen) {
                found = card;
                break;
            }
        }
        if (found == null) {
            for (CardInstance card : getActiveState().getCards(Location.DISCARD_PILE, CardType.ANY)) {
                if (GameStateAdaptor.getEnum(card) == chosen) {
                    found = card;
                    break;
                }
            }
        }
        choiceHandler.put(ChoiceType.CHOOSE_CARD, found);
        getActiveState().unmaskAllDice();
        startChain(new ActionActivateCardsDisc(getActiveState()));
    }

    @Override
    public void activateMoneyDisc(int diceToUse) throws UnsupportedOperationException {
        Dice toUse = getActiveState().getDiceOfValue(diceToUse);
        choiceHandler.put(ChoiceType.CHOOSE_ACTION_DICE, toUse);
        startChain(new ActionActivateMoneyDisc(getActiveState()));
    }

    @Override
    public CardActivator activateBribeDisc(int diceToUse) throws UnsupportedOperationException {
        Action toExecute = new ActionActivateBribeDisc(getActiveState());
        Dice toUse = getActiveState().getDiceOfValue(diceToUse);
        choiceHandler.put(ChoiceType.CHOOSE_ACTION_DICE, toUse);
        getActiveState().maskDice (getActiveState().getActionDiceOfValue(getActiveState().getDiceDiscOfValue(diceToUse)));
        return new Activator(choiceHandler, getActiveState(), theirState.getPlayerCardsOnDiscs(theirState.getWhoseTurn())[6], toExecute, this);
    }

    @Override
    public void endTurn() throws UnsupportedOperationException {
        startChain(new ActionEndTurn(getActiveState()));
    }

    @Override
    public void placeCard(Card toPlace, int discToPlaceOn) throws UnsupportedOperationException {
        for (CardInstance card : getActiveState().getCards(Location.PLAYER_HAND, CardType.ANY)) {
            if (card.getGameCard().getClass() == GameStateAdaptor.getGameCard(toPlace).getClass() && !choiceHandler.availableSelections.contains(card)) {
                choiceHandler.put(ChoiceType.PLACE_CARD, card);
                break;
            }
        }
        choiceHandler.put(ChoiceType.CHOOSE_DICE_DISC, getActiveState().getDiceDiscOfValue(discToPlaceOn));
        startChain(new ActionPlaceCard(getActiveState()));
    }

    @Override
    public LocalGameState getActiveState() {
        return theirState.getGameState();
    }

    @Override
    public void setActiveState(LocalGameState ourState) {
        this.theirState.setGameState(ourState);
    }

    static class LinearChoiceInterface implements ChoiceHandler , ChoiceAdapter {

        final Queue<Object> availableSelections;
        final Queue<ChoiceAdapter> isolatedAdapters;

        public LinearChoiceInterface() {
            this.availableSelections = new LinkedList<Object>();
            this.isolatedAdapters = new LinkedList<ChoiceAdapter>();
        }

        @Override
        public void put(ChoiceType type, Object result) {
            assert (result != null);
            this.availableSelections.add(result);
        }

        @Override
        public void purge () {
            this.availableSelections.clear();
        }

        @Override
        public void dealWithChoices(List<Choice> choices) {

            while (isolatedAdapters.peek() != null) {
                availableSelections.addAll(isolatedAdapters.poll().getBuffer());
            }

            //System.out.print("    Choices in queue: ");
            for (Object o : availableSelections) {
                //System.out.print(o + " ");
            }
            //System.out.println();

            assert (!availableSelections.isEmpty());

            for (Object eventual : availableSelections) {

                //System.out.println("#Considering for a " + eventual);

                //System.out.println("###Possible Choices: ");
                for (Choice choice : choices) {
                    //System.out.println("###" + choice.getValue());

                    if ((choice.getValue() instanceof Token &&
                            eventual instanceof Token &&
                            ((Token)choice.getValue()).equals((Token)eventual)) ||
                            choice.getValue().equals(eventual)) {
                        choice.getOwner().chosen(choice);
                        availableSelections.remove(eventual);
                        //System.out.println("Chosen: " + choice.getValue());
                        return;
                    }
                }
            }
            //System.out.println("Required Choice not found in queue");
            assert (false);
        }

        @Override
        public Queue<Object> getBuffer() {
            return availableSelections;
        }

        public ChoiceAdapter getIsolatedBuffer() {
            isolatedChoiceAdapter adaptor = new isolatedChoiceAdapter();
            this.isolatedAdapters.add(adaptor);
            return adaptor;
        }

        @Override
        public void report(String report) {
        }
    }
}

class Activator implements
        AesculapinumActivator,
        ArchitectusActivator,
        CardActivator,
        CenturioActivator,
        ConsiliariusActivator,
        ConsulActivator,
        EssedumActivator,
        ForumActivator,
        GladiatorActivator,
        HaruspexActivator,
        LegatActivator,
        LegionariusActivator,
        MachinaActivator,
        MercatorActivator,
        MercatusActivator,
        NeroActivator,
        OnagerActivator,
        PraetorianusActivator,
        ScaenicusActivator,
        SenatorActivator,
        SicariusActivator,
        TelephoneBoxActivator,
        TribunusPlebisActivator,
        VelitesActivator {

    private final ChoiceAdapter choicesAdaptor;
    private final LocalGameState state;
    private final GameStateAdaptor stateAdaptor;
    private final Card card;
    private final Action startOfChain;
    private final Mover mover;

    private boolean valid = true;
    private final LinearChoiceInterface choicesInterface;

    private boolean forumSecondDiceChoiceMade = false;

    public Activator(LinearChoiceInterface choicesMap, LocalGameState state, Card card, Action startOfChain, Mover parent) {
        this.choicesInterface = choicesMap;
        this.choicesAdaptor = choicesInterface.getIsolatedBuffer();
        this.state = state;
        this.stateAdaptor = new GameStateAdaptor(state);
        this.card = card;
        this.startOfChain = startOfChain;
        this.mover = parent;

    }

    @Override
    public void chooseCardFromPile(int indexOfCard) {
        List<CardInstance> cards = null;
        if (card == Card.AESCULAPINUM) {
            cards = (List) state.getCards(Location.DISCARD_PILE, CardType.ANY);
        } else if (card == Card.HARUSPEX) {
            cards = (List) state.getCards(Location.DRAW_PILE, CardType.ANY);
        }
        choicesAdaptor.put(ChoiceType.CHOOSE_CARD, cards.get(indexOfCard));
    }

    public void markAsInvalid () {
        this.valid = false;
    }

    @Override
    public void complete() {

        state.unmaskAllDice();

        if (card == Card.SENATOR || card == Card.ARCHITECTUS) {
            choicesAdaptor.put(ChoiceType.TRUE_FALSE, false);
        }

        if (card == Card.FORUM && !forumSecondDiceChoiceMade) {
            choicesAdaptor.put(ChoiceType.TRUE_FALSE, false);
        }

        if (startOfChain != null && valid) {
            mover.startChain(startOfChain);
        }
    }

    @Override
    public void chooseCenturioAddActionDie(boolean attackAgain) {
        choicesAdaptor.put(ChoiceType.TRUE_FALSE, attackAgain);
    }

    @Override
    public void giveAttackDieRoll(int roll) {
        state.setNextBattleDie(roll);
    }

    @Override
    public void chooseActionDice(int actionDiceValue) {
        Dice toUse = state.getDiceOfValue(actionDiceValue);
        assert (toUse != null);
        choicesAdaptor.put(ChoiceType.CHOOSE_ACTION_DICE, toUse);
        state.maskDice(toUse);
    }

    @Override
    public void placeCard(Card card, int diceDisc) {
        chooseCard(card, Location.DICE_DISC);

        choicesAdaptor.put(ChoiceType.CHOOSE_DICE_DISC, state.getDiceDiscOfValue(diceDisc));
    }

    private void chooseCard(Card toLay, Location location) {
        Class<? extends GameCard> toMatch = GameStateAdaptor.getGameCard(toLay).getClass();
        for (CardInstance c : state.getCards(location, CardType.ANY)) {
            if (c.getGameCard().getClass() == toMatch && (!choicesAdaptor.getBuffer().contains(c) || (card == GameStateAdaptor.getEnum(c)))) {
                choicesAdaptor.put(ChoiceType.CHOOSE_CARD, c);
                return;
            }
        }
        assert (false);
    }

    @Override
    public void chooseConsulChangeAmount(int amount) {
        if (amount > 0) {
            choicesAdaptor.put(ChoiceType.CHANGE_ACTION_DIE, 1);
        } else {
            choicesAdaptor.put(ChoiceType.CHANGE_ACTION_DIE, -1);
        }
    }

    @Override
    public void chooseWhichDiceChanges(int originalRoll) {
        Dice toUse = state.getDiceOfValue(originalRoll);
        choicesAdaptor.put(ChoiceType.CHANGE_ACTION_DIE, toUse);
        state.maskDice(toUse);
    }

    @Override
    public void chooseActivateTemplum(boolean activate) {
        forumSecondDiceChoiceMade = true;
        choicesAdaptor.put(ChoiceType.TRUE_FALSE, true);
    }

    @Override
    public void chooseActivateTemplum(int diceValue) {

        if (!forumSecondDiceChoiceMade) {
            choicesAdaptor.put(ChoiceType.TRUE_FALSE, true);
            forumSecondDiceChoiceMade = true;
        }

        Dice toUse = state.getDiceOfValue(diceValue);
        choicesAdaptor.put(ChoiceType.CHOOSE_ACTION_DICE, toUse);
        state.maskDice(toUse);
    }

    @Override
    public void chooseDiceDisc(int diceDisc) {

        if (card == Card.SICARIUS || card == Card.VELITES || card == Card.ONAGER || card == Card.NERO || card == Card.GLADIATOR) {
            choicesAdaptor.put(ChoiceType.CHOOSE_CARD, state.getCardsOnDisc(state.getDiceDiscOfValue(diceDisc), state.getOpponents(state.getCurrentPlayer()).iterator().next()).iterator().next());
        } else if (card == Card.TELEPHONEBOX) {
            choicesAdaptor.put(ChoiceType.CHOOSE_CARD, state.getCardsOnDisc(state.getDiceDiscOfValue(diceDisc), state.getCurrentPlayer()).iterator().next());
        } else {
            choicesAdaptor.put(ChoiceType.CHOOSE_DICE_DISC, state.getDiceDiscOfValue(diceDisc));
        }
    }

    @Override
    public void chooseMercatorBuyNum(int VPToBuy) {

        for (int i = 0; i < VPToBuy; i++) {
            choicesAdaptor.put(ChoiceType.TRUE_FALSE, true);
        }
        choicesAdaptor.put(ChoiceType.TRUE_FALSE, false);
    }

    @Override
    public CardActivator getScaenicusMimicTarget(int diceDisc) {
        choicesAdaptor.put(ChoiceType.CHOOSE_CARD, state.getCardsOnDisc(state.getDiceDiscOfValue(diceDisc), state.getCurrentPlayer()).iterator().next());
        return new Activator(choicesInterface, state, stateAdaptor.getPlayerCardsOnDiscs(stateAdaptor.getWhoseTurn())[diceDisc - 1], null, mover);
    }

    @Override
    public void shouldMoveForwardInTime(boolean isForward) {
        choicesAdaptor.put (ChoiceType.TRUE_FALSE, isForward);
    }

    @Override
    public void setSecondDiceUsed(int diceValue) {
        Dice toUse = state.getDiceOfValue(diceValue);
        choicesAdaptor.put(ChoiceType.CHANGE_ACTION_DIE, toUse);
        state.maskDice(toUse);
    }

    @Override
    public void layCard(Card myCard, int whichDiceDisc) {
        choicesAdaptor.put(ChoiceType.TRUE_FALSE, true);
        chooseCard(myCard, Location.PLAYER_HAND);
        choicesAdaptor.put(ChoiceType.CHOOSE_DICE_DISC, state.getDiceDiscOfValue(whichDiceDisc));
    }
}

interface ChoiceAdapter {

    void put (ChoiceType type, Object object);
    Queue<Object> getBuffer ();
    void purge();

}

class isolatedChoiceAdapter implements ChoiceAdapter {

    final Queue<Object> choiceBuffer;

    public isolatedChoiceAdapter() {
        this.choiceBuffer = new LinkedList<Object>();
    }

    @Override
    public void put(ChoiceType type, Object object) {
        assert (object != null);
        this.choiceBuffer.add(object);
    }

    @Override
    public Queue<Object> getBuffer() {
        return this.choiceBuffer;
    }

    @Override
    public void purge() {
        this.choiceBuffer.clear();
    }
}