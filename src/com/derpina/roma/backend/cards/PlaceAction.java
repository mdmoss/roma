/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;


import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.*;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class PlaceAction extends RealAction {

    protected CardInstance card;
    protected DiceDisc disc;

    public PlaceAction(CardInstance card, LocalGameState gameState, DiceDisc disc) {
        super(gameState);
        this.card = card;
        this.disc = disc;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
    }

    @Override
    public Choice registerAction(AbstractChoiceFactory owner) {
        return new Choice(ChoiceType.PLACE_CARD, "Place " + card.getName(), this, owner);
    }
}
