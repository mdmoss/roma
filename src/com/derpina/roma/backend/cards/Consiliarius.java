/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.ActionLayFloatingCard;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionActivateRearranger;
import com.derpina.roma.core.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Consiliarius extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateRearranger (gameState, cardInstance, CardType.CHARACTER);
    }

    @Override
    public String getName() {
        return "Consiliarius";
    }

    @Override
    public String getCaption() {
        return "The player picks up their character cards and can then lay them"
                + " again on any dice disc. Buildings can be covered";
    }

    @Override
    public int getBaseCost() {
        return 4;
    }

    @Override
    public int getBaseDefense() {
        return 4;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}
