/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.ChoiceType;
import com.derpina.roma.core.DiceDisc;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public abstract class ActivateAction extends RealAction {

    CardInstance card;
    DiceDisc disc;

    public ActivateAction(LocalGameState gameState, CardInstance card) {
        super(gameState);
        this.card = card;
        disc = gameState.getCardLocation(card);
    }

    public void setActivatingCard (CardInstance card) {
        this.card = card;
    }

    @Override
    public boolean isValid() {
        return true;

    }

    @Override
    public void execute() {
    }
}
