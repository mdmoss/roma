/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.core.*;

/**
 *
 * @author Matt
 */
public abstract class AbstractCard implements GameCard {

    @Override
    public PlaceAction getPlaceAction(LocalGameState gameState, DiceDisc disc, CardInstance card) {
        return new PlaceAction(card, gameState, disc);
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance card) {
        return new NonActivatable(gameState, card);
    }

    @Override
    public Action getDiscardAction(LocalGameState gameState, CardInstance card) {
        return new DiscardAction(gameState, card);
    }

    @Override
    public CardInstance createInstance() {
        return new NotAbstractCardInstance(this) {};
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
