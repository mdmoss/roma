/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;
import com.derpina.roma.core.Rules;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Legat extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateLegat(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Legat";
    }

    @Override
    public String getCaption() {
        return "A player gets one victory point from the stockpile for each"
                + "dice disc not occupied by the opponent.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateLegat extends ActivateAction {

    public ActionActivateLegat(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public void execute() {
        int points = Rules.NUM_CARD_FIELDS - gameState.getOpposingCards(Location.DICE_DISC, CardType.ANY).size();
        gameState.movePointsFromPool(gameState.getCurrentPlayer(), points);
        executeReport = gameState.getCurrentPlayer() + " won " + points + " points for the Opponent's empty Dice Discs";
    }
}