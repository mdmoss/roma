package com.derpina.roma.backend.cards;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionCardAttack;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Legionarius extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateLegionarius(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Legionarius";
    }

    @Override
    public String getCaption() {
        return "Attacks the opponent's card which is directly opposite,"
                + "whether it is a character or building.";
    }

    @Override
    public int getBaseCost() {
        return 4;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateLegionarius extends ActivateAction {

    public ActionActivateLegionarius(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public void execute() {
        super.execute();
        if (!gameState.getOppositeLaidCards(card, CardType.ANY).isEmpty()) {
            this.addNextAction(new ActionCardAttack(gameState.getOppositeLaidCards(card, CardType.ANY).iterator().next(), gameState));
        }
    }
}