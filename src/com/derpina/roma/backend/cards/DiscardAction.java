/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.actions.literal.ActionLiteralPutCardInDiscardPile;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class DiscardAction extends RealAction {
    protected final CardInstance target;

    public DiscardAction(LocalGameState gameState, CardInstance target) {
        super(gameState);
        this.target = target;
    }

    @Override
    public boolean isValid() {
        return (gameState.getOwner(target) != null);
    }

    @Override
    public void execute() {
        this.addNextAction(new ActionLiteralPutCardInDiscardPile(gameState, target));
    }
}
