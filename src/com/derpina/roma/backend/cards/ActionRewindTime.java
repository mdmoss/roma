/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.Dice;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class ActionRewindTime extends RealAction {

    CardInstance target;
    Dice activateValue;
    DiceDisc landingZone;

    public ActionRewindTime(CardInstance target, Dice activateValue, LocalGameState gameState) {
        super(gameState);
        this.target = target;
        this.activateValue = activateValue;
        this.landingZone = gameState.getCardLocation(target);
    }

    public int getRequiredTimeTravel() {
        return Math.max(gameState.getValueOf(activateValue), gameState.getCurrentTurnNo());
    }

    public CardInstance getTarget() {
        return this.target;
    }

    public DiceDisc getLandingZone() {
        return this.landingZone;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
    }
}
