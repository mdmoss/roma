/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.backend.actions.literal.ActionLiteralPutCardInDiscardPile;
import com.derpina.roma.core.*;


/**
 *
 * @author Matt
 */
public class Kat extends AbstractCard {

    @Override
    public String getName() {
        return "Kat";
    }

    @Override
    public String getCaption() {
        return "Mysterious and revered animal.  Its haunting cry lifts "
                + "the heart of all who hear it.  Has nine lives.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 1;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new NonActivatable(gameState, cardInstance);
    }

    @Override
    public Action getDiscardAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionDiscardKat(gameState, cardInstance);
    }

    @Override
    public CardInstance createInstance() {
        return new KatInstance(this);
    }


}
class ActionDiscardKat extends DiscardAction {

    public ActionDiscardKat(LocalGameState gameState, CardInstance target) {
        super(gameState, target);
    }

    @Override
    public void execute() {
        if (gameState.getOwner(target) != gameState.getCurrentPlayer()) {
            ((KatInstance)target).takeLife();
            if (((KatInstance)target).getLives() < 1) {
                this.addNextAction(new ActionLiteralPutCardInDiscardPile(gameState, target));
                ((KatInstance)target).setLives(Rules.KAT_LIVES);
            }
        } else {
            this.addNextAction(new ActionLiteralPutCardInDiscardPile(gameState, target));
            ((KatInstance)target).setLives(Rules.KAT_LIVES);
        }

    }
}

class KatInstance extends NotAbstractCardInstance {

    private int lives = Rules.KAT_LIVES;

    public KatInstance(GameCard card) {
        super(card);
    }

    public void setLives (int lives) {
        this.lives = lives;
    }

    public void takeLife () {
        this.lives--;
    }

    public int getLives () {
        return this.lives;
    }

    @Override
    public NotAbstractCardInstance clone() {

        KatInstance newKat = new KatInstance(this.getGameCard());
        newKat.lives = this.lives;
        return newKat;
    }
}

