/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.ArbitraryCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.Location;
import java.util.ArrayList;
import java.util.Collection;



/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Scaenicus extends AbstractCard {


    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateScaenicus(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Scaenicus";
    }

    @Override
    public String getCaption() {
        return "He performs no action of his own, but can copy the action"
                + " of any of the player's own face up character cards, "
                + "and the next time around that of another";
    }

    @Override
    public int getBaseCost() {
        return 8;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateScaenicus extends ActivateAction {

    ArbitraryCardFactory cardtoMimic;

    public ActionActivateScaenicus(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        cardtoMimic = new ArbitraryCardFactory(gameState, getMimicableCards());
        registerChoice(cardtoMimic);
    }

    @Override
    public boolean isValid() {
        return !getMimicableCards().isEmpty();
    }

    private Collection<CardInstance> getMimicableCards() {
        Collection<CardInstance> result = new ArrayList<CardInstance>();
        for (CardInstance c : gameState.getCards(Location.DICE_DISC, CardType.CHARACTER)) {
            if (c.getGameCard().getClass() != Scaenicus.class && gameState.getActionDice().size() >= c.getNumDiceRequired()) {
                ActivateAction activateAction = (ActivateAction) c.getActivateAction(gameState, c);
                activateAction.setActivatingCard(this.card);

                if (activateAction.isValid()) {
                    result.add(c);
                }
            }
        }

        return result;
    }

    @Override
    public void execute() {
        ActivateAction action = (ActivateAction) cardtoMimic.getValue().getActivateAction(gameState, cardtoMimic.getValue());
        action.setActivatingCard(this.card);
        executeReport = gameState.getCurrentPlayer() + " chose to mimic their " + cardtoMimic.getValue().getName();
        this.forceNextAction(action);
    }

}