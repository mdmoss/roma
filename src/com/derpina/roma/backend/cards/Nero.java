/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.OpposingLaidCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class Nero extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateNero(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Nero";
    }

    @Override
    public String getCaption() {
        return "Destroys any face-up opposing building card. The destroyed"
                + " card and Nero are both discarded.";
    }

    @Override
    public int getBaseCost() {
        return 8;
    }

    @Override
    public int getBaseDefense() {
        return 9;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateNero extends ActivateAction {

    OpposingLaidCardFactory building;

    public ActionActivateNero(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        building = new OpposingLaidCardFactory(gameState, CardType.BUILDING);
        this.registerChoice(building);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !gameState.getOpposingCards(Location.DICE_DISC, CardType.BUILDING).isEmpty();
    }

    @Override
    public void execute() {
        super.execute();

        gameState.moveCardToDiscardPile(building.getValue());
        gameState.moveCardToDiscardPile(card);
        executeReport = gameState.getCurrentPlayer() + " burnt down their Opponent's " + building.getValue().toString();
    }

    @Override
    public Collection<Choice> getIntents() {
        return building.getChoices();
    }
}