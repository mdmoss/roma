/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionActivateTelephoneBox;
import com.derpina.roma.core.*;

/**
 *
 * @author Damon Stacey
 */
public class TelephoneBox extends AbstractCard {

    @Override
    public String getName() {
        return "Telephone Box";
    }

    @Override
    public String getCaption() {
        return "When activated by an action die the telephone Box card "
                + "sends one of the owner's cards already on the board "
                + "forwards or backwards in time.  The sent card is "
                + "called the time-traveling card.Using the Telephone "
                + "Box card requires two of the three action dice, "
                + "one to activate the Telephone Box card on its dice "
                + "disc in the usual way, and one to determine n how "
                + "many turns forwards or backwards in time the "
                + "time-traveling card is to be sent.  The player "
                + "selects which is to be the time-traveling card to be "
                + "send through time, and in which direction it is to "
                + "be sent: forwards or backwards.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 1;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateTelephoneBox(gameState, cardInstance);
    }
}


