/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionContinueFreePlace;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.core.*;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Senetor extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateSenetor(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Senetor";
    }

    @Override
    public String getCaption() {
        return "Enables the player to lay as many character cards as "
                + "they wish free of charge";
    }

    @Override
    public int getBaseCost() {
        return 3;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateSenetor extends ActivateAction {

    public ActionActivateSenetor(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public boolean isValid() {
        boolean result = false;
        for (CardInstance c : gameState.getCards(Location.PLAYER_HAND, CardType.CHARACTER)) {
            if (c.getType() == CardType.CHARACTER) {
                result = true;
                break;
            }
        }

        return (super.isValid() && result);
    }

    @Override
    public void execute() {
        this.addNextAction(new ActionContinueFreePlace(gameState, CardType.CHARACTER));
    }
}
