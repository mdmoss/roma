/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionDiscardCard;
import com.derpina.roma.backend.choosers.OpposingLaidCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Sicarius extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateSicarius(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Sicarius";
    }

    @Override
    public String getCaption() {
        return "Eliminates an opposing, face-up character card. The opposing card and Sicarius are both discarded";
    }

    @Override
    public int getBaseCost() {
        return 9;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateSicarius extends ActivateAction {

    OpposingLaidCardFactory toRemove;

    public ActionActivateSicarius(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        toRemove =  new OpposingLaidCardFactory(gameState, CardType.ANY);
        this.registerChoice(toRemove);
    }

    @Override
    public void execute() {
        super.execute();
        this.addNextAction(new ActionDiscardCard(gameState, toRemove.getValue()));
        this.addNextAction(new ActionDiscardCard(gameState, card));
        executeReport = gameState.getCurrentPlayer() + " assasinated the Opponent's " + toRemove.getValue();
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !(gameState.getOpposingCards(Location.DICE_DISC, CardType.CHARACTER).isEmpty());
    }
}