/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 * @author Matthew Moss
 */
public class GrimReaper extends AbstractCard {

    @Override
    public String getName() {
        return "Grim Reaper";
    }

    @Override
    public String getCaption() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new NonActivatable(gameState, cardInstance);
    }

}
