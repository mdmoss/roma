/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.choosers.BooleanChoiceFactory;
import com.derpina.roma.core.*;
import com.derpina.roma.utils.InstantCollection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Mercator extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateMercator(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Mercator";
    }

    @Override
    public String getCaption() {
        return "For two sestertii each, the player can buy 1 victory point from"
                + " their opponent, as long as there are money and victory"
                + " points left! The opponent gets the money";
    }

    @Override
    public int getBaseCost() {
        return 7;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateMercator extends ActivateAction {

    ActionMercatorBuy startOfChain;

    public ActionActivateMercator(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        startOfChain = new ActionMercatorBuy(gameState);
    }

    @Override
    public List<Action> next() {
        if (startOfChain.isValid()) {
            return InstantCollection.get(startOfChain);
        } else {
            return new ArrayList<Action>();
        }
    }
}

class ActionMercatorBuy extends RealAction {

    BooleanChoiceFactory buy;

    public ActionMercatorBuy(LocalGameState gameState) {
        super(gameState);
        buy = new BooleanChoiceFactory(gameState,
                      "Buy (another) victory point (2 Sestertii)", "No Thanks");
        this.registerChoice(buy);
    }

    @Override
    public boolean isValid() {

        boolean otherPlayerHasPoints = false;
        for (Player p : gameState.getOpponents(gameState.getCurrentPlayer())) {
            if (gameState.getPlayerPoints(p) >= Rules.MERCATOR_VP_GAIN) {
                otherPlayerHasPoints = true;
                break;
            }
        }

        return (gameState.getPlayerMoney(gameState.getCurrentPlayer())
                >= Rules.MERCATOR_SESTERTII_TRADE_AMOUNT
                && otherPlayerHasPoints);
    }

    @Override
    public void execute() {
        if (buy.getValue()) {
            Player buyFrom = gameState.getOpponents(gameState.getCurrentPlayer()).iterator().next();//FIXME haxky hacks until posssible change is known
            gameState.moveVictoryPoints(buyFrom, gameState.getCurrentPlayer(), Rules.MERCATOR_VP_GAIN);
            gameState.spendPlayerMoney(gameState.getCurrentPlayer(), Rules.MERCATOR_SESTERTII_TRADE_AMOUNT);
            gameState.earnPlayerMoney(buyFrom, Rules.MERCATOR_SESTERTII_TRADE_AMOUNT);
            executeReport = gameState.getCurrentPlayer() + " purchased " + Rules.MERCATOR_VP_GAIN + " points from their Opponent for " + Rules.MERCATOR_SESTERTII_TRADE_AMOUNT + " sestertii";
        }
    }

    @Override
    public List<Action> next() {
        if (new ActionMercatorBuy(gameState).isValid() && buy.getValue() == true) {
        return InstantCollection.get(new ActionMercatorBuy(gameState));
        } else {
            return super.next();
        }
    }
}