/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.ArbitraryCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Choice;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.Location;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class Aesculapinum extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateAesculapinum(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Aesculapinum";
    }

    @Override
    public String getCaption() {
        return "The temple of Asculapius (the got of healing) enables"
                + " the player to pick up any character from the discard"
                + " pile and add it to their hand.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateAesculapinum extends ActivateAction {

    ArbitraryCardFactory choice;

    public ActionActivateAesculapinum(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        choice = new ArbitraryCardFactory(gameState, gameState.getCards(Location.DISCARD_PILE, CardType.CHARACTER));
        this.registerChoice(choice);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !gameState.getCards(Location.DISCARD_PILE, CardType.CHARACTER).isEmpty();
    }

    @Override
    public void execute() {
        super.execute();
        gameState.moveCardToHand(choice.getValue(), gameState.getCurrentPlayer());
        executeReport = gameState.getCurrentPlayer() + " moved a " + choice.getValue().getName() + " to their hand";
    }
}