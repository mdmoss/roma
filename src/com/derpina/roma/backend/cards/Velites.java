/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionCardAttack;
import com.derpina.roma.backend.choosers.OpposingLaidCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Matt
 */
public class Velites extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateVelites(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Velites";
    }

    @Override
    public String getCaption() {
        return "Attacks any opposing character card (does not have to"
                + "be directly opposite). The battle die is thrown once.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateVelites extends ActivateAction {

    OpposingLaidCardFactory toAttack;

    public ActionActivateVelites(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        toAttack = new OpposingLaidCardFactory(gameState, CardType.CHARACTER);
        this.registerChoice(toAttack);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !(gameState.getOpposingCards(Location.DICE_DISC, CardType.CHARACTER).isEmpty());
    }

    @Override
    public void execute() {
        super.execute();
        this.addNextAction(new ActionCardAttack(toAttack.getValue(), gameState));
    }


}