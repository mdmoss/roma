/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;

/**
 *
 * @author Matt
 */
public class Basilica extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new NonActivatable(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Basilica";
    }

    @Override
    public String getCaption() {
        return "If a forum is activated (it must lie directly next to "
                + "the basilica), the player gets 2 more victory points."
                + " The Basilica itself is not activated";
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}
