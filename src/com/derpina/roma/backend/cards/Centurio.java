/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionDiscardCard;
import com.derpina.roma.backend.actions.ActionPrerollBattleDie;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.BooleanChoiceFactory;
import com.derpina.roma.core.Dice;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;

/**
 *
 * @author Matt
 */
public class Centurio extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateCenturio(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Centurio";
    }

    @Override
    public String getCaption() {
        return "attacks the card directly opposite, whether it is a "
                + "character or building card. The value of an unused "
                + "action die can be added to the value of the baddle "
                + "die (the action die is then counted as used). This "
                + "is decided after the battle die has been thrown.";
    }

    @Override
    public int getBaseCost() {
        return 9;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateCenturio extends ActivateAction {

    Dice attackRoll;

    public ActionActivateCenturio(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && (!gameState.getOppositeLaidCards(card, CardType.ANY).isEmpty());
    }

    @Override
    public void execute() {
        super.execute();
        attackRoll = gameState.getBattleDie();
        this.forceNextAction(new ActionDecideContinueCenturioAttack(gameState, card, attackRoll));
        this.addNextAction(new ActionPrerollBattleDie(gameState));
        executeReport = gameState.getCurrentPlayer() + "attacked with a Centurio, and did " + gameState.getValueOf(attackRoll) + " damage";
    }

}


class ActionDecideContinueCenturioAttack extends RealAction {

    BooleanChoiceFactory decision;
    private final CardInstance attacker;
    Dice prevRoll;

    public ActionDecideContinueCenturioAttack(LocalGameState gameState, CardInstance attacker, Dice prevRoll) {
        super(gameState);

        this.decision = new BooleanChoiceFactory(gameState, "Add an Action Die to the attack roll", "Accept defeat");
        this.registerChoice(decision);
        this.attacker = attacker;
        this.prevRoll = prevRoll;
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        if (decision.getValue() == true && !gameState.getActionDice().isEmpty()) {
            this.forceNextAction(new ActionContinueCenturioAttack(gameState, attacker, prevRoll));
        } else {
            for (CardInstance c : gameState.getOppositeLaidCards(attacker, CardType.ANY)) {
                if (gameState.getValueOf(prevRoll) >= gameState.getDefenceFor(c, gameState)) {
                    this.addNextAction(new ActionDiscardCard(gameState, c));
                    executeReport = gameState.getCurrentPlayer() + "'s attack was succesful!";
                }
                executeReport = gameState.getCurrentPlayer() + "'s attack failed";
            }
        }
    }
}

class ActionContinueCenturioAttack extends RealAction {

    ActionDiceFactory dice;
    private final CardInstance attacker;
    Dice prevRoll;

    public ActionContinueCenturioAttack(LocalGameState gameState, CardInstance attacker, Dice prevRoll) {
        super(gameState);
        this.attacker = attacker;
        this.prevRoll = prevRoll;

        this.dice = new ActionDiceFactory(gameState);
        this.registerChoice(dice);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {

        for (CardInstance c : gameState.getOppositeLaidCards(attacker, CardType.ANY)) {
            if (gameState.getValueOf(prevRoll) + gameState.getValueOf(dice.getValue()) >= gameState.getDefenceFor(c, gameState)) {
                gameState.useUpDice(dice.getValue());
                this.addNextAction(new ActionDiscardCard(gameState, c));
                executeReport = gameState.getCurrentPlayer() + " added an action dice of value + " + gameState.getValueOf(dice.getValue()) + " and their attack was succesful";
            }
            executeReport = gameState.getCurrentPlayer() + "'s attack failed";
        }

    }
}