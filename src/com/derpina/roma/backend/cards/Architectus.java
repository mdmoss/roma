/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionContinueFreePlace;
import com.derpina.roma.backend.actions.ActionFreePlaceType;
import com.derpina.roma.backend.modifiers.AbstractModifier;
import com.derpina.roma.backend.modifiers.CostModifier;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.DiceDisc;
import com.derpina.roma.core.Location;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Matt
 */
public class Architectus extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateArchitectus(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Architectus";
    }

    @Override
    public String getCaption() {
        return "enables the player to lay as many building cards as "
                + "they wish free of charge. The player is allowed to "
                + "cover any cards.";
    }

    @Override
    public int getBaseCost() {
        return 3;
    }

    @Override
    public int getBaseDefense() {
        return 4;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateArchitectus extends ActivateAction {

    public ActionActivateArchitectus(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public boolean isValid() {

        return (super.isValid() &&
                !gameState.getCards(
                    Location.PLAYER_HAND,
                    CardType.BUILDING).isEmpty());
    }

    @Override
    public void execute() {
        this.addNextAction(new ActionContinueFreePlace(gameState, CardType.BUILDING));
    }
}
