package com.derpina.roma.backend.cards;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.core.*;

/**
 *
 * @author Matt
 */
public class TribunusPlebis extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateTribunusPlebis(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Tribunus Plebis";
    }

    @Override
    public String getCaption() {
        return "The player gets 1 victory point from their opponent";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateTribunusPlebis extends ActivateAction {

    public ActionActivateTribunusPlebis(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public void execute() {
        super.execute();
        for (Player p : gameState.getOpponents(gameState.getCurrentPlayer())) {
            gameState.moveVictoryPoints(p, gameState.getCurrentPlayer(), Rules.TRIBUNUS_PLEBIS_TRANSFER_AMOUNT);
            executeReport = gameState.getCurrentPlayer() + " took " + Rules.TRIBUNUS_PLEBIS_TRANSFER_AMOUNT + " from their Opponent";
        }
    }
}
