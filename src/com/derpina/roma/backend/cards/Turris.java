/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.LocalGameState;


/**
 *
 * @author Matt
 */
public class Turris extends AbstractCard {

    @Override
    public String getName() {
        return "Turris";
    }

    @Override
    public String getCaption() {
        return "As long as the Turris is face up, the defence value of all the player's other face up cards increases by 1";
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 6;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new NonActivatable(gameState, cardInstance);
    }
}