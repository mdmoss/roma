/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.OpposingLaidCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Gladiator extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateGladiator(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Gladiator";
    }

    @Override
    public String getCaption() {
        return "An opponent's face-up character card (chosen by the player whose"
                + "turn it is) must be returned to the opponent's hand.";
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateGladiator extends ActivateAction {

    OpposingLaidCardFactory cardChoice;

    public ActionActivateGladiator(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        cardChoice = new OpposingLaidCardFactory(gameState, CardType.CHARACTER);
        this.registerChoice(cardChoice);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !gameState.getOpposingCards(Location.DICE_DISC, CardType.CHARACTER).isEmpty();
    }

    @Override
    public void execute() {
        super.execute();

        if (!gameState.getIsBlocked(gameState.getCardLocation(card), gameState.getCurrentPlayer(), gameState)) {
            gameState.moveCardToHand(cardChoice.getValue(), gameState.getOwner(cardChoice.getValue()));
            executeReport = gameState.getCurrentPlayer() + " send the Opponent's " + cardChoice.getValue().getName() + " back to their hand";
        }
    }
}