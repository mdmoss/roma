/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.DiceDiscFactory;
import com.derpina.roma.backend.modifiers.AbstractModifier;
import com.derpina.roma.backend.modifiers.DiscModifier;
import com.derpina.roma.core.*;

/**
 *
 * @author Matt
 */
public class Praetorianus extends AbstractCard {

    PraetorianusDiscBlocker mod;

    @Override
    public String getName() {
        return "Praetorianus";
    }

    @Override
    public String getCaption() {
        return "Any of the opponent's dice disc can be blocked for one go.";
    }

    @Override
    public int getBaseCost() {
        return 4;
    }

    @Override
    public int getBaseDefense() {
        return 4;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivatePraetorianus(gameState, cardInstance);
    }
}

class ActionActivatePraetorianus extends ActivateAction {

    DiceDiscFactory target;

    public ActionActivatePraetorianus(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        target = new DiceDiscFactory(gameState);
        this.registerChoice(target);
    }

    @Override
    public void execute() {
        super.execute();
        gameState.getDiscModifiers().add(new PraetorianusDiscBlocker(target.getValue(), gameState.getCurrentPlayer(), gameState.getCurrentTurnNo()));
        executeReport = gameState.getCurrentPlayer() + " blocked Dice Disc " + gameState.getDiscIntegerValue(target.getValue());
    }
}

class PraetorianusDiscBlocker extends AbstractModifier implements DiscModifier {

    DiceDisc target;
    Player owner;
    int createTurn;

    public PraetorianusDiscBlocker(DiceDisc target, Player owner, int currentTurn) {
        this.target = target;
        this.owner = owner;
        createTurn = currentTurn;
    }

    @Override
    public DiscModifier clone() {
        PraetorianusDiscBlocker result = new PraetorianusDiscBlocker(target, owner, createTurn);
        return result;
    }

    @Override
    public void mutate(LocalGameState gameState) {
        if (gameState.getCurrentTurnNo() != createTurn &&
            gameState.getCurrentPlayer() == owner) {
            gameState.getDiscModifiers().remove(this);
        }
    }

    @Override
    public boolean isBlocked(LocalGameState gameState, boolean previous, DiceDisc disc, Player who) {
        if (disc == target && who != owner) {
            return true;
        } else {
            return previous;
        }
    }
}