/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionActivateRearranger;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;

/**
 *
 * @author Matt
 */
public class Machina extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateRearranger(gameState, cardInstance, CardType.BUILDING);
    }

    @Override
    public String getName() {
        return "Machina";
    }

    @Override
    public String getCaption() {
        return "The player picks up their building cards and lays hem again on any dice disc. Character cards can be covered.";
    }

    @Override
    public int getBaseCost() {
        return 4;
    }

    @Override
    public int getBaseDefense() {
        return 4;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}
