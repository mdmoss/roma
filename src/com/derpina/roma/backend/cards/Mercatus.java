/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;
import com.derpina.roma.core.Rules;

/**
 *
 * @author Matt
 */
public class Mercatus extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateMercatus(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Mercatus";
    }

    @Override
    public String getCaption() {
        return "The player gets 1 victory point for every face-up Forum that the opponent has.";
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }

}

class ActionActivateMercatus extends ActivateAction {

    public ActionActivateMercatus(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public void execute() {
        super.execute();
        int numForums = 0;
        for (CardInstance c : gameState.getOpposingCards(Location.DICE_DISC, CardType.BUILDING)) {
            if (c.getGameCard().getClass() == Forum.class) {
                gameState.moveVictoryPoints(gameState.getOwner(c),gameState.getCurrentPlayer(), Rules.MERCATUS_VP_GAIN);
                numForums++;
            }
        }
        executeReport = gameState.getCurrentPlayer() + " gained " + numForums + "points for the Opponent's Forums";
    }
}