/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.ArbitraryCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Matt
 */
public class Haruspex extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateHaruspex(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Haruspex";
    }

    @Override
    public String getCaption() {
        return "The player can choose any card from the pile of face-down "
                + "cards and add it to their hand. Afterwards, the pile is shuffled";
    }

    @Override
    public int getBaseCost() {
        return 4;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateHaruspex extends ActivateAction {

    ArbitraryCardFactory drawFactory;

    public ActionActivateHaruspex(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        drawFactory = new ArbitraryCardFactory(gameState, gameState.getCards(Location.DRAW_PILE, CardType.ANY));
        this.registerChoice(drawFactory);
    }

    @Override
    public void execute() {
        super.execute();
        gameState.moveCardToHand(drawFactory.getValue(), gameState.getOwner(card));

        // Shuffle the deck
        for (CardInstance c : gameState.getCards(Location.DRAW_PILE, CardType.ANY)) {
            gameState.makeCardFloating(c);
        }

        List<CardInstance> toShuffle = new LinkedList<CardInstance>(gameState.getCards(Location.FLOATING, CardType.ANY));
        Collections.shuffle(toShuffle);

        for (CardInstance c : toShuffle) {
            gameState.moveCardToDrawPile(c);
        }

        executeReport = gameState.getCurrentPlayer() + " took a card, and the deck was shuffled";
    }

    @Override
    public boolean isValid() {
        boolean cardsDrawable = gameState.getCards(Location.DRAW_PILE, CardType.ANY).size() > 0;
        return super.isValid() && (cardsDrawable);
    }
}