/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.NonActivatable;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;

/**
 *
 * @author Matt
 */
public class Templum extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new NonActivatable(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Templum";
    }

    @Override
    public String getCaption() {
        return "If a forum is activated (it must lie directly next to "
                + "the Templum), the third action die can be used to "
                + "determine the number of additional victory points "
                + "which the player gets from the general stockpile. "
                + "The action die must not yet have been used in this "
                + "go. The Templum itself is not activated separately.";
    }

    @Override
    public int getBaseCost() {
        return 2;
    }

    @Override
    public int getBaseDefense() {
        return 2;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}
