/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.ActionCardAttack;
import com.derpina.roma.backend.choosers.OpposingLaidCardFactory;
import com.derpina.roma.core.CardInstance;
import com.derpina.roma.core.CardType;
import com.derpina.roma.core.Location;

/**
 *
 * @author Matt
 */
public class Onager extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateOnager(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Onager";
    }

    @Override
    public String getCaption() {
        return "This Roman catapult attacks any opposing building. "
                + "The battle die is thrown once";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 4;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}

class ActionActivateOnager extends ActivateAction {

    OpposingLaidCardFactory toAttack;

    public ActionActivateOnager(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        toAttack = new OpposingLaidCardFactory(gameState, CardType.BUILDING);
        this.registerChoice(toAttack);
    }

    @Override
    public boolean isValid() {
        return super.isValid() && !(gameState.getOpposingCards(Location.DICE_DISC, CardType.BUILDING).isEmpty());
    }

    @Override
    public void execute() {
        this.addNextAction(new ActionCardAttack(toAttack.getValue(), gameState));
    }
}