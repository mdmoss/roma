package com.derpina.roma.backend.cards;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.modifiers.AbstractModifier;
import com.derpina.roma.backend.modifiers.DefenceModifier;
import com.derpina.roma.core.*;


/**
 *
 * @author Matt
 */
public class Essedum extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateEssedum(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Essedum";
    }

    @Override
    public String getCaption() {
        return "The defence value of the opponent's face up cards is"
                + "reduced by 2.";
    }

    @Override
    public int getBaseCost() {
        return 6;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 0;
    }
}
class ActionActivateEssedum extends ActivateAction {

    public ActionActivateEssedum(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
    }

    @Override
    public void execute() {
        gameState.getDefenceModifiers().add(new EssedumDefenceModifier(gameState.getCurrentPlayer(), card.getGameCard()));
        executeReport = gameState.getCurrentPlayer() + "lowered their opponent's defense";
    }
}

class EssedumDefenceModifier extends AbstractModifier implements DefenceModifier {

    private final Player owner;
    private final GameCard source;

    public EssedumDefenceModifier(Player owner, GameCard source) {
        this.owner = owner;
        this.source = source;
    }

    @Override
    public DefenceModifier clone() {
        return new EssedumDefenceModifier(owner, source);
    }

    @Override
    public void mutate(LocalGameState gameState) {
        gameState.getDefenceModifiers().remove(this);
    }

    @Override
    public int modifyDefence(LocalGameState gameState, int original, CardInstance card) {
        Player holder = gameState.getOwner(card);

        boolean essedumStillOnBoard = false;

        for (CardInstance laidCard : gameState.getCards(Location.DICE_DISC, CardType.ANY)) {
            if (laidCard.getGameCard() == source) {
                essedumStillOnBoard = true;
            }
        }

        if (holder != null && !holder.equals (owner) && essedumStillOnBoard) {
            return original - Rules.ESSEDUM_DEFENCE_REDUCTION;
        } else {
            return original;
        }
    }
}