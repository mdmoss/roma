/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.DiceRoller;
import com.derpina.roma.core.LocalGameState;
import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.AbstractChoiceFactory;
import com.derpina.roma.core.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author Lasath Fernando <edu@lasath.org>
 */
public class Consul extends AbstractCard {

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateConsul(gameState, cardInstance);
    }

    @Override
    public String getName() {
        return "Consul";
    }

    @Override
    public String getCaption() {
        return "The score on an action dice which has not been used can be"
                + " increased or decreased by one point";
    }

    @Override
    public int getBaseCost() {
        return 3;
    }

    @Override
    public int getBaseDefense() {
        return 3;
    }

    @Override
    public CardType getType() {
        return CardType.CHARACTER;
    }

    @Override
    public int getNumDiceRequired() {
        return 1;
    }
}


class ActionActivateConsul extends ActivateAction {

    ActionDiceFactory dice;
    UpDownFactory direction;


    public ActionActivateConsul(LocalGameState gameState, CardInstance card) {
        super(gameState, card);

        this.dice = new ActionDiceFactory(gameState);
        this.registerChoice(dice);
        this.direction = new UpDownFactory(gameState);
        this.registerChoice(direction);
    }

    @Override
    public void execute() {
        super.execute();
        final int oldValue = gameState.getValueOf(dice.getValue());
        if (oldValue == 1) {
            gameState.setValueOfDice(dice.getValue(), oldValue + Rules.CONSUL_INCREASE_AMOUNT);
        } else if (oldValue == Rules.NUM_DICE_SIDES) {
            gameState.setValueOfDice(dice.getValue(), oldValue - Rules.CONSUL_INCREASE_AMOUNT);
        } else {
            gameState.setValueOfDice(dice.getValue(), oldValue + direction.getValue());
        }
        executeReport = gameState.getCurrentPlayer() + " changed the value of an Action Die";
    }

    @Override
    public boolean isValid() {
        return (gameState.getActionDice().size() >= 1);
    }

    @Override
    public Collection<Choice> getIntents() {
        if (!dice.isValueSet()) {
            return dice.getChoices();
        } else if (!(gameState.getValueOf(dice.getValue()) == 1 ||
                gameState.getValueOf(dice.getValue()) == Rules.NUM_DICE_SIDES)) {
            return direction.getChoices();
        } else {
            return new ArrayList<Choice>();
        }
    }
}

class UpDownFactory extends AbstractChoiceFactory<Integer> {

    public UpDownFactory(LocalGameState gameState) {
        super(gameState);
    }

    @Override
    protected void populateChoices(Collection<Choice> choices) {
        choices.add(new Choice(ChoiceType.CHANGE_ACTION_DIE, "Increase by one", new Integer(1), this));
        choices.add(new Choice(ChoiceType.DECREASE_ACTION_DIE, "Decrease by one", new Integer(-1), this));
    }

}