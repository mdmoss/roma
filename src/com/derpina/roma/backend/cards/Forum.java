/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.backend.actions.RealAction;
import com.derpina.roma.backend.choosers.ActionDiceFactory;
import com.derpina.roma.backend.choosers.BooleanChoiceFactory;
import com.derpina.roma.core.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author Matt
 */
public class Forum extends AbstractCard {

    @Override
    public String getName() {
        return "Forum";
    }

    @Override
    public String getCaption() {
        return "requires 2 action dice: one to activate the Forum and "
                + "the other to determine how many victory points "
                + "the player recieves.";
    }

    @Override
    public int getBaseCost() {
        return 5;
    }

    @Override
    public int getBaseDefense() {
        return 5;
    }

    @Override
    public CardType getType() {
        return CardType.BUILDING;
    }

    @Override
    public int getNumDiceRequired() {
        return 1;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance cardInstance) {
        return new ActionActivateForum (gameState, cardInstance);
    }
}

class ActionActivateForum extends ActivateAction {

    ActionDiceFactory diceToUse;

    ActionActivateForum(LocalGameState gameState, CardInstance card) {
        super(gameState, card);
        diceToUse = new ActionDiceFactory(gameState);
        this.registerChoice(diceToUse);
    }

    @Override
    public void execute() {
        super.execute();

        int pointsGain = gameState.getValueOf(diceToUse.getValue());

        for (CardInstance c : gameState.getAdjacentCards(card)) {
            if (c.getGameCard().getClass() == Basilica.class) {
                pointsGain += Rules.BASILICA_VP_GAIN_INCREASE;
                executeReport = gameState.getCurrentPlayer() + " activated a Forum with a Basilica for " + pointsGain + "points";
            } else {
                executeReport = gameState.getCurrentPlayer() + " activated a Forum for " + pointsGain + "points";
            }
        }
        gameState.useUpDice(diceToUse.getValue());
        gameState.movePointsFromPool(gameState.getCurrentPlayer(), pointsGain);
    }

    @Override
    public List<Action> next() {

        List<Action> result = new ArrayList<Action>();
        if (!gameState.getActionDice().isEmpty()) {
            for (CardInstance c : gameState.getAdjacentCards(card)) {
                if (c.getGameCard().getClass() == Templum.class) {
                    result.add(new ActionAddTemplumDice(gameState));
                    break;
                }
            }
        }
        return result;
    }
}

class ActionAddTemplumDice extends RealAction {

    BooleanChoiceFactory choice;
    ActionDiceFactory dice;

    public ActionAddTemplumDice(LocalGameState gameState) {
        super(gameState);
        choice = new BooleanChoiceFactory(gameState, "Add another dice (Templum)", "Don't add a second dice");
        this.registerChoice(choice);
        dice = new ActionDiceFactory(gameState);
        this.registerChoice(dice);
    }

    @Override
    public boolean isValid() {
        return true;
    }

    @Override
    public void execute() {
        if (choice.getValue() == true) {
            gameState.movePointsFromPool(gameState.getCurrentPlayer(), gameState.getValueOf(dice.getValue()));
        }
    }

    @Override
    public Collection<Choice> getIntents() {
        if (!choice.isValueSet()) {
            return choice.getChoices();
        } else if (choice.getValue() == true) {
            return dice.getChoices();
        } else {
            return new ArrayList<Choice>();
        }
    }
}