package com.derpina.roma.backend.cards;

import com.derpina.roma.backend.actions.Action;
import com.derpina.roma.core.*;

/**
 *
 * @author Matt
 */
public class NotAbstractCardInstance implements CardInstance {

    private GameCard electorate;

    public NotAbstractCardInstance(GameCard card) {
        this.electorate = card;
    }

    public NotAbstractCardInstance(CardInstance other) {
        this.electorate = ((NotAbstractCardInstance) other).electorate;
    }

    @Override
    public String getName() {
        return electorate.getName();
    }

    @Override
    public String getCaption() {
        return electorate.getCaption();
    }

    @Override
    public CardType getType() {
        return electorate.getType();
    }

    @Override
    public int getBaseCost() {
        return electorate.getBaseCost();
    }

    @Override
    public int getBaseDefense() {
        return electorate.getBaseDefense();
    }

    @Override
    public int getNumDiceRequired() {
        return electorate.getNumDiceRequired();
    }

    @Override
    public GameCard getGameCard() {
        return electorate;
    }

    @Override
    public Action getActivateAction(LocalGameState gameState, CardInstance card) {
        return electorate.getActivateAction(gameState, card);
    }

    @Override
    public Action getDiscardAction(LocalGameState gameState, CardInstance card) {
        return electorate.getDiscardAction(gameState, card);
    }

    @Override
    public PlaceAction getPlaceAction(LocalGameState gameState, DiceDisc disc, CardInstance card) {
        return electorate.getPlaceAction(gameState, disc, card);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotAbstractCardInstance other = (NotAbstractCardInstance) obj;
        if (this.electorate != other.electorate) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.electorate != null ? this.electorate.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public void setToken(GameCard electorate) {
        this.electorate = electorate;
    }

    @Override
    public NotAbstractCardInstance clone() {
        return new NotAbstractCardInstance(this);
    }
}