


import com.derpina.roma.backend.Mover;
import com.derpina.roma.backend.gamestate.AdvancedGameState;
import com.derpina.roma.backend.gamestate.GameStateAdaptor;
import com.derpina.roma.backend.gamestate.RomaBuilder;
import framework.interfaces.AcceptanceInterface;
import framework.interfaces.GameState;
import framework.interfaces.MoveMaker;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Matt
 */
public class AcceptanceProvider implements AcceptanceInterface {

    @Override
    public MoveMaker getMover(GameState state) {
        return Mover.getInstance(state);
    }

    @Override
    public GameState getInitialState() {
        AdvancedGameState g = new AdvancedGameState(new RomaBuilder());

        return new GameStateAdaptor(g);
    }

}
